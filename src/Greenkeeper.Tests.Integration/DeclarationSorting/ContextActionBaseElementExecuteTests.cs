﻿using System.Collections.ObjectModel;
using Greenkeeper.DeclarationSorting;
using JetBrains.ProjectModel;
using JetBrains.ReSharper.Feature.Services.CSharp.Bulbs;
using JetBrains.ReSharper.Intentions.CSharp.Test;
using JetBrains.ReSharper.Intentions.Extensibility;
using JetBrains.TextControl;
using NUnit.Framework;

namespace Greenkeeper.Tests.Integration.DeclarationSorting
{
    [TestFixture]
    public class ContextActionBaseElementExecuteTests : CSharpContextActionExecuteTestBase<DeclarationSorterContextAction>
    {
        //BasePathForFilesUnderTest: test\data\Intentions\ContextActions\ + ExtraPath
        protected override string ExtraPath
        {
            get { return "DeclarationSorterBaseElementExecution"; }
        }

        protected override DeclarationSorterContextAction CreateContextAction(ISolution solution, ITextControl textControl)
        {
            var dataProvider = CSharpContextActionDataProviderFactory.Create(solution, textControl);
            var comparer = new DeclarationBaseElementComparer(new Collection<string>());
            var declarationSorter = DeclarationSorterFactory.CreateDeclarationSorter(comparer);
            return DeclarationSorterContextAction.Create(dataProvider, declarationSorter);
        }

        [TestCase("ExecutionOnUnsortedWithOneInterface")]
        [TestCase("ExecutionOnUnsortedWithOneBaseClass")]
        [TestCase("ExecutionOnUnsortedWithTwoInterfaces1")]
        [TestCase("ExecutionOnUnsortedWithTwoInterfaces2")]
        [TestCase("ExecutionOnUnsortedWithOneBaseClassAndOneInterface1")]
        [TestCase("ExecutionOnUnsortedWithOneBaseClassAndOneInterface2")]
        [TestCase("ExecutionOnUnsortedWithOverlappingInterfaces")]
        [TestCase("ExecutionOnUnsortedWithHierarchicInterfaces1")]
        [TestCase("ExecutionOnUnsortedWithHierarchicInterfaces2")]
        [TestCase("ExecutionOnUnsortedWithHierarchicInterfaces3")]
        [TestCase("ExecutionOnUnsortedWithOneAbstractBaseClass")]
        [TestCase("ExecutionOnUnsortedWithOverlappingInterfacesOrderSwitched")]
        [TestCase("ExecutionOnUnsortedWithOneInterfaceNotDeclared")]
        [TestCase("ExecutionOnUnsortedWithTwoInterfacesNotDeclared")]
        [TestCase("ExecutionOnUnsortedWithTwoOverlappingInterfacesNotDeclared")]
        [TestCase("ExecutionOnUnsortedWithInterfaceIncludesMethodsAndProperties")]
        [TestCase("ExecutionOnUnsortedWithTwoInterfacesWithSameName")]
        [TestCase("ExecutionOnUnsortedWithOverridingObject")]
        [TestCase("ExecutionOnUnsortedWithInterfaceIncludesOnlyProperties")]
        [TestCase("ExecutionOnUnsortedWithXmlSerializableInterface")]
        public void Test(string fileName)
        {
            DoOneTest(fileName);
        }
    }
}
