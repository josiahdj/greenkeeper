﻿using System;
using Greenkeeper.DeclarationSorting;
using JetBrains.Annotations;

namespace Greenkeeper.Tests.Integration.DeclarationSorting
{
    public static class DeclarationSorterFactory
    {
        public static IDeclarationSorter CreateDeclarationSorter([NotNull] IDeclarationComparer comparer)
        {
            if (comparer == null) throw new ArgumentNullException("comparer");
            return new DeclarationSorter(comparer);
        }
    }
}
