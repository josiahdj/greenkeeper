﻿using System.Collections.ObjectModel;
using Greenkeeper.DeclarationSorting;
using JetBrains.ProjectModel;
using JetBrains.ReSharper.Intentions.CSharp.Test;
using JetBrains.TextControl;
using NUnit.Framework;

namespace Greenkeeper.Tests.Integration.DeclarationSorting
{
    [TestFixture]
    public class ContextActionBaseElementWithExcludedTypesExecuteTests : CSharpContextActionExecuteTestBase<DeclarationSorterContextAction>
    {
        //BasePathForFilesUnderTest: test\data\Intentions\ContextActions\ + ExtraPath
        protected override string ExtraPath
        {
            get { return "DeclarationSorterBaseElementExecutionWithExcludedTypes"; }
        }

        [TestCase("ExecutionOnUnsortedWithOneInterfaceAndDisposable")]
        [TestCase("ExecutionOnUnsortedWithOneInterfaceAndObject")]
        [TestCase("ExecutionOnUnsortedWithOneInterfaceAndObjectAndDisposable")]
        public void Test(string fileName)
        {
            DoOneTest(fileName);
        }
        
        protected override DeclarationSorterContextAction CreateContextAction(ISolution solution, ITextControl textControl)
        {
            var dataProvider = CSharpContextActionDataProviderFactory.Create(solution, textControl);
            var comparer = new DeclarationBaseElementComparer(new Collection<string> { "System.IDisposable", "System.Object" });
            var declarationSorter = DeclarationSorterFactory.CreateDeclarationSorter(comparer);
            return DeclarationSorterContextAction.Create(dataProvider, declarationSorter);
        }
    }
}