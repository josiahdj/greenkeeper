﻿using Greenkeeper.DeclarationSorting;
using Greenkeeper.DeclarationSorting.Serialization;
using JetBrains.ProjectModel;
using JetBrains.ReSharper.Intentions.CSharp.Test;
using JetBrains.TextControl;
using NUnit.Framework;

namespace Greenkeeper.Tests.Integration.DeclarationSorting.Serialization
{
    [TestFixture]
    public class ContextActionSerializedDefaultExecuteTests : CSharpContextActionExecuteTestBase<DeclarationSorterContextAction>
    {
        //BasePathForFilesUnderTest: test\data\Intentions\ContextActions\ + ExtraPath
        protected override string ExtraPath
        {
            get { return "DeclarationSorterDefaultExecution"; }
        }

        protected override DeclarationSorterContextAction CreateContextAction(ISolution solution, ITextControl textControl)
        {
            var dataProvider = CSharpContextActionDataProviderFactory.Create(solution, textControl);
            var deserializer = new DeclarationSortingDefinitionSerializer(DefaultDeclarationSortingDefinitionResource.DefaultDeclarationSortingDefinition);
            var definition = deserializer.Deserialize().Create();

            return DeclarationSorterContextAction.Create(dataProvider, definition.DeclarationSorter, definition.AdditionalExecutor);
        }
       
        [TestCase("ExecutionOnUnsortedTestClass")]
        [TestCase("ExecutionOnGenericClassWithInterface")]
        public void Test(string fileName)
        {
            DoOneTest(fileName);
        }

    }
}
