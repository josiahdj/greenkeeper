﻿using NUnit.Framework;

namespace Greenkeeper.Tests.Integration.DeclarationSorting.Serialization
{
   
    [TestFixture]
    public class ContextActionSerializedNoClassDeclarationConditionExecuteTests : ContextActionSerializedExecuteTestBase
    {
        //BasePathForFilesUnderTest: test\data\Intentions\ContextActions\ + ExtraPath
        protected override string ExtraPath
        {
            get { return "DeclarationSorterNoClassDeclarationConditionExecution"; }
        }

        protected override string XmlFileName
        {
            get { return "SorterWithNoClassDeclarationCondition"; }
        }
        
        [TestCase("ExecutionOnUnsorted1")]
        public void Test(string fileName)
        {
            DoOneTest(fileName);
        }
    }
}
