﻿using Greenkeeper.DeclarationSorting;
using Greenkeeper.DeclarationSorting.Serialization;
using JetBrains.ProjectModel;
using JetBrains.ReSharper.Feature.Services.CSharp.Bulbs;
using JetBrains.ReSharper.Intentions.CSharp.Test;
using JetBrains.ReSharper.Intentions.Extensibility;
using JetBrains.TextControl;
using NUnit.Framework;

namespace Greenkeeper.Tests.Integration.DeclarationSorting.Serialization
{
    [TestFixture]
    public class ContextActionSerializedStyleCopDefaultExecuteTests : CSharpContextActionExecuteTestBase<DeclarationSorterContextAction>
    {
        protected override string ExtraPath
        {
            get { return "DeclarationSorterStyleCopDefaultExecution"; }
        }
        
        protected override DeclarationSorterContextAction CreateContextAction(ISolution solution, ITextControl textControl)
        {
            var dataProvider = CSharpContextActionDataProviderFactory.Create(solution, textControl);
            var deserializer = new DeclarationSortingDefinitionSerializer(DefaultDeclarationSortingDefinitionResource.StyleCopDeclarationSortingDefinition);
            var definition = deserializer.Deserialize().Create();

            return DeclarationSorterContextAction.Create(dataProvider, definition.DeclarationSorter, definition.AdditionalExecutor);
        }
       
        [TestCase("ExecutionOnUnsorted")]
        public void Test(string fileName)
        {
            DoOneTest(fileName);
        }
    }

}