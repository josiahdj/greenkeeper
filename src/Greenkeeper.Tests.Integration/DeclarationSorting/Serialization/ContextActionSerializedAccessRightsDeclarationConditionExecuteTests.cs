﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Greenkeeper.Tests.Integration.DeclarationSorting.Serialization
{
    [TestFixture]
    public class ContextActionSerializedAccessRightsDeclarationConditionExecuteTests : ContextActionSerializedExecuteTestBase
    {
        protected override string ExtraPath
        {
            get { return "DeclarationSorterAccessRightsConditionExecution"; }
        }

        protected override string XmlFileName
        {
            get { return "SorterWithAccessRightsCondition"; }
        }

        [TestCase("ExecutionOnUnsortedMembers")]
        public void Test(string fileName)
        {
            DoOneTest(fileName);
        }
    }
}
