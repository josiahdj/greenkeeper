class {caret} A
{
  private void A()
  {
  
  }

  private void X()
  {
  
  }

  internal void C()
  {
	A();
	D();
  }

  protected void B()
  {
	A();
  }

  public void H()
  {
	Y();
	X();
  }
  private void E()
  {
	A();
	B();
	N();
  }

  private void D()
  {
	H();
  }

  private void N()
  {
  
  }

  private void Z()
  {
  
  }

  private void Y()
  {
	Z();
  }
}