class A
{
  public A(string arg1, string arg2{caret})
	:this(arg1,arg2)
  {
    
  }
  
  public A(string arg1, string arg2, string arg3)
  {
	if (arg1 == null) throw new ArgumentNullException("arg1");
	if (arg2 == null) throw new ArgumentNullException("arg2");
	if (arg3 == null) throw new ArgumentNullException("arg3");
  }
}