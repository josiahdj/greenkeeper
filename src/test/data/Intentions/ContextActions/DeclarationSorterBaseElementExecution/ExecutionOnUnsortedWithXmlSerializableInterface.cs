using System;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

public class {caret} XmlSerializable<T> : IXmlSerializable where T : class
{
    private const XmlSchema _UNKNOWN_XML_SCHEMA = null;
    public T Serialized { get; set; }

    public XmlSchema GetSchema()
    {
        return _UNKNOWN_XML_SCHEMA;
    }

    public T GetSerialized()
    {
        if (Serialized == null) throw new ArgumentNullException("Serialized");
        return Serialized;
    }

    public void ReadXml(XmlReader reader)
    {
        if (reader == null) throw new ArgumentNullException("reader");
        throw new NotImplementedException();
    }

    public void WriteXml(XmlWriter writer)
    {
        if (writer == null) throw new ArgumentNullException("writer");
        throw new NotImplementedException();
    }
}