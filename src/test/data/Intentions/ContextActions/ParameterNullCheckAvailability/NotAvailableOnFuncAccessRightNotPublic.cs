class A
{
  A(string arg1{off})
  {
  } 
  
  private A(string arg1{off})
  {
  } 
  
  internal A(string arg1{off})
  {
  } 
  
  protected A(string arg1{off})
  {
  } 
  
  virtual A(string arg1{off})
  {
  } 

  void A(string arg1{off})
  {
  }  
  
  private void A(string arg1{off})
  {
  }  
  
  internal void A(string arg1{off})
  {
  }  
  
  protected void A(string arg1{off})
  {
  } 
  
  virtual void A(string arg1{off})
  {
  } 
}
