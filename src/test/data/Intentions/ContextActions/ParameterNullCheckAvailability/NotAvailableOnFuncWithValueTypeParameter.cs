class A
{
  public A(int arg1{off})
  {	
  }  
  
  public A(int arg1, string arg2{off})
  {	
	if (null == arg2) throw new ArgumentNullException("arg2");
  } 
  
  public A(int arg1, string arg2{on})
  {		
  } 
  
  public void A(int arg1{off})
  {	
  }   
  
  public void A(int arg1, string arg2{off})
  {	
	if (null == arg2) throw new ArgumentNullException("arg2");
  } 
  
  public void A(int arg1, string arg2{on})
  {		
  } 
}