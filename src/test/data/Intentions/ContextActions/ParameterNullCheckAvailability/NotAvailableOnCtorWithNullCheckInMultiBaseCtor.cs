class A : B
{
  public A(string arg1{off})
	: base("",arg1)
  {   
  
  }   
}

class B : C
{
  public B(string a1, string a2)
	:base("","",a2)
  {    	
  } 
}

class C : D
{
  public B(string a1, string a2, string a3)
	:base("","","",a3)
  {    	
  } 
}

class D
{
  public B(string a1, string a2, string a3, string a4)
  {    
	if (a4 == null) throw new ArgumentNullException("a4");
  } 
}