class A : B
{
  public A(string arg1,string arg2{off})
	: base(arg2,arg1)
  {   
  
  }   
}

class B : C
{
  public B(string a1, string a2)
	:base("","",a2)
  {    	
	if (a1 == null) throw new ArgumentNullException("a1");
  } 
}

class C : D
{
  public B(string a1, string a2, string a3)
	:base("","","",a3)
  {    	
  } 
}

class D
{
  public B(string a1, string a2, string a3, string a4)
  {    
	if (a4 == null) throw new ArgumentNullException("a4");
  } 
}