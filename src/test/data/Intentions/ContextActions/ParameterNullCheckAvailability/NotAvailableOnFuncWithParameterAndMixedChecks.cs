class A
{
  public A(string arg1,string arg2,string arg3,string arg4,string arg5,string arg6,string arg7,string arg8{off})
  {
    if (String.IsNullOrEmpty(arg1)) throw new ArgumentNullException("arg1");
	if (string.IsNullOrEmpty(arg2)) throw new ArgumentNullException("arg2");
	if (String.IsNullOrWhiteSpace(arg3)) throw new ArgumentNullException("arg3");
	if (string.IsNullOrWhiteSpace(arg4)) throw new ArgumentNullException("arg4");
	if (ReferenceEquals(arg5, null)) throw new ArgumentNullException("arg5");
	if (ReferenceEquals(null, arg6)) throw new ArgumentNullException("arg6");
	if (arg7 == null) throw new ArgumentNullException("arg7");
	if (null == arg8) throw new ArgumentNullException("arg8");
  }
  
  public void B(string arg1,string arg2,string arg3,string arg4,string arg5,string arg6,string arg7,string arg8{off})
  {
    if (String.IsNullOrEmpty(arg1)) throw new ArgumentNullException("arg1");
	if (string.IsNullOrEmpty(arg2)) throw new ArgumentNullException("arg2");
	if (String.IsNullOrWhiteSpace(arg3)) throw new ArgumentNullException("arg3");
	if (string.IsNullOrWhiteSpace(arg4)) throw new ArgumentNullException("arg4");
	if (ReferenceEquals(arg5, null)) throw new ArgumentNullException("arg5");
	if (ReferenceEquals(null, arg6)) throw new ArgumentNullException("arg6");
	if (arg7 == null) throw new ArgumentNullException("arg7");
	if (null == arg8) throw new ArgumentNullException("arg8");
  }
}

class B
{
  public B(string arg1,string arg2,string arg3,string arg4,string arg5,string arg6,string arg7,string arg8{off})
  {
    if (String.IsNullOrEmpty(arg1))
    {
        throw new ArgumentNullException("arg1");
    }

	if (string.IsNullOrEmpty(arg2))
	{
	    throw new ArgumentNullException("arg2");
	}

	if (String.IsNullOrWhiteSpace(arg3))
	{
	    throw new ArgumentNullException("arg3");
	}

	if (string.IsNullOrWhiteSpace(arg4))
	{
	    throw new ArgumentNullException("arg4");
	}

	if (ReferenceEquals(arg5, null))
	{
	    throw new ArgumentNullException("arg5");
	}

	if (ReferenceEquals(null, arg6))
	{
	    throw new ArgumentNullException("arg6");
	}

	if (arg7 == null)
	{
	    throw new ArgumentNullException("arg7");
	}

	if (null == arg8)
	{
	    throw new ArgumentNullException("arg8");
	}
  }
  
  public void A(string arg1,string arg2,string arg3,string arg4,string arg5,string arg6,string arg7,string arg8{off})
  {
    if (String.IsNullOrEmpty(arg1))
    {
        throw new ArgumentNullException("arg1");
    }

	if (string.IsNullOrEmpty(arg2))
	{
	    throw new ArgumentNullException("arg2");
	}

	if (String.IsNullOrWhiteSpace(arg3))
	{
	    throw new ArgumentNullException("arg3");
	}

	if (string.IsNullOrWhiteSpace(arg4))
	{
	    throw new ArgumentNullException("arg4");
	}

	if (ReferenceEquals(arg5, null))
	{
	    throw new ArgumentNullException("arg5");
	}

	if (ReferenceEquals(null, arg6))
	{
	    throw new ArgumentNullException("arg6");
	}

	if (arg7 == null)
	{
	    throw new ArgumentNullException("arg7");
	}

	if (null == arg8)
	{
	    throw new ArgumentNullException("arg8");
	}
  }
}