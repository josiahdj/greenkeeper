class A
{ 
  public A(string arg1, string arg2{on})
  {
    if (arg1 == null) throw new ArgumentNullException("arg1");
  }
  
  public B(string arg1, string arg2{on})
  {
    if (arg2 == null) throw new ArgumentNullException("arg2");
  }
  
  public A(string arg1, string arg2, string arg3{on})
  {
    if (arg1 == null) throw new ArgumentNullException("arg1");
	if (arg2 == null) throw new ArgumentNullException("arg2");	
  }
  
  public B(string arg1, string arg2, string arg3{on})
  {
    if (arg2 == null) throw new ArgumentNullException("arg2");
	if (arg3 == null) throw new ArgumentNullException("arg3");	
  }
  
  public C(string arg1, string arg2, string arg3{on})
  {
    if (arg1 == null) throw new ArgumentNullException("arg1");
	if (arg3 == null) throw new ArgumentNullException("arg3");	
  }
  
  public void A(string arg1, string arg2{on})
  {
    if (arg1 == null) throw new ArgumentNullException("arg1");
  }
  
  public void B(string arg1, string arg2{on})
  {
    if (arg2 == null) throw new ArgumentNullException("arg2");
  }
  
  public void A(string arg1, string arg2, string arg3{on})
  {
    if (arg1 == null) throw new ArgumentNullException("arg1");
	if (arg2 == null) throw new ArgumentNullException("arg2");	
  }
  
  public void B(string arg1, string arg2, string arg3{on})
  {
    if (arg2 == null) throw new ArgumentNullException("arg2");
	if (arg3 == null) throw new ArgumentNullException("arg3");	
  }
  
  public void C(string arg1, string arg2, string arg3{on})
  {
    if (arg1 == null) throw new ArgumentNullException("arg1");
	if (arg3 == null) throw new ArgumentNullException("arg3");	
  }  
}