class {caret} A
{
  private void A()
  {
  
  }

  private void X()
  {
  
  }

  private void C()
  {
	A();
	D();
  }

  private void B()
  {
	A();
  }

  private void H()
  {
	Y();
	X();
  }
  private void E()
  {
	A();
	B();
	N();
  }

  private void D()
  {
	H();
  }

  private void N()
  {
  
  }

  private void Z()
  {
  
  }

  private void Y()
  {
	Z();
  }
}