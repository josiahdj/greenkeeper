class {caret} A
{
  private void A()
  {
  
  }

  private void C()
  {
	A();
	D();
  }

  private void E()
  {
	A();
  }

  private void D()
  {
  
  }
}