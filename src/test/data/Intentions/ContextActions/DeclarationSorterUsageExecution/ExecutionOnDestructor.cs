class {caret} A
{
	private static void B()
	{	
	}
	
	private void C()
	{	
	}
	
	public ~A()
	{
		C();
		B();
	}
}