class {caret} A
{
	private ICollection<string> ItemsA;
	private ICollection<string> ItemsB;

	public string this[int index]
	{
		get { return ItemsB[index] ?? ItemsA[index]; }
	}
}