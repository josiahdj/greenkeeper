class {caret} A
{
  public void A(string arg)
  {
  
  }

  public void E()
  {
  
  }

  internal string I {get; set;}

  public string F {get; set;}

  internal string G {get; set;}

  public void X()
  {
  
  }

  public string _TestA;

  public string _TestC;

  internal void D()
  {
  
  }

  internal void S()
  {
  
  }

  protected string CB;

  internal void W()
  {
  
  }

  protected void C()
  {
  
  }

  protected void N()
  {
  
  }

  protected void O()
  {
  
  }

  protected void P()
  {
  
  }

  private void A()
  {
  
  }

  private void M()
  {
  
  }

  protected string AB;

  internal string K {get; set;}

  private string U {get; set;}

  private void Z()
  {
  
  }

  private string _TestB;
}