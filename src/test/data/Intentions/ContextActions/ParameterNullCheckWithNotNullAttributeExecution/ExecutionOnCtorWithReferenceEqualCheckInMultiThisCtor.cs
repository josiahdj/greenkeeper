﻿class A
{
  public A(string arg1,string arg2,int number{caret})
	: this(arg2,arg1)
  {    
  }
  
  public A(string a1, string a2)
	: this(a1,"",a2)
  { 	
  } 
  
  public A(string a1, string a2, string a3)
	: this("","","",a3)
  {    	
  } 
  
  public A(string a1, string a2, string a3, string a4)
  {    	
	if (ReferenceEquals(a4, null)) throw new ArgumentNullException("a4");
  }
}