﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Greenkeeper.DeclarationSorting;
using JetBrains.ReSharper.Psi.Tree;
using NUnit.Framework;
using Rhino.Mocks;

namespace Greenkeeper.Tests.DeclarationSorting
{
    [TestFixture]
    public class MultiDeclarationComparerTests
    {
        [TestCase(0, 0, 0)]
        [TestCase(1, -1, 1)]
        [TestCase(0, 1, 1)]
        public void CompareTest(int firstResult, int secResult, int result)
        {
            var firstMock = GetMockedDeclarationComparer(firstResult);
            var secMock = GetMockedDeclarationComparer(secResult);

            var multiComparer =
                new MultiDeclarationComparer(new Collection<IDeclarationComparer> { firstMock, secMock });

            multiComparer.InitWithDeclarationOrderElement(new List<DeclarationOrderElement>());

            Assert.AreEqual(result, multiComparer.Compare(GetMockedDeclaration(), GetMockedDeclaration()));
        }

        private IDeclarationComparer GetMockedDeclarationComparer(int result)
        {
            var mocked = MockRepository.GenerateMock<IDeclarationComparer>();
            mocked.Stub(c => c.Compare(null, null)).IgnoreArguments().Return(result).Repeat.Once();
            return mocked;
        }

        private DeclarationOrderElement GetMockedDeclaration()
        {
            return new DeclarationOrderElement(MockRepository.GenerateMock<IDeclaration>());
        }
    }
}
