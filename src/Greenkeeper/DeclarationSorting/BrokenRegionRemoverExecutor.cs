﻿using System.Collections.Generic;
using JetBrains.ReSharper.Psi.CSharp.Tree;

namespace Greenkeeper.DeclarationSorting
{
    public class BrokenRegionRemoverExecutor : DefaultAdditionalExecutor
    {
        private ICollection<RegionDeclarationRange> _RegionDeclarationRangesBeforeSort;
        private ICollection<RegionDeclarationRange> _RegionDeclarationRangesAfterSort;

        public override void BeforeSort(IClassDeclaration classDeclaration)
        {
            _RegionDeclarationRangesBeforeSort = DetectRegionDeclarationRange(classDeclaration);
        }

        public override void AfterSort(IClassDeclaration classDeclaration)
        {
            _RegionDeclarationRangesAfterSort = DetectRegionDeclarationRange(classDeclaration);
            RemoveBrokenRegions();
        }

        private void RemoveBrokenRegions()
        {
            var remover = new BrokenRegionRemover(_RegionDeclarationRangesBeforeSort, _RegionDeclarationRangesAfterSort, TreeNodeWriter);
            remover.RemoveBrokenRegions();
        }

        private ICollection<RegionDeclarationRange> DetectRegionDeclarationRange(IClassDeclaration classDeclaration)
        {
            var detector = RegionDeclarationRangeDetector.CreateRegionDeclarationRangeDetector(classDeclaration);
            return detector.Detect();
        }
    }
}
