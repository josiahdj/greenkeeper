﻿using JetBrains.ReSharper.Psi.CSharp.Tree;

namespace Greenkeeper.DeclarationSorting
{
    public class NoClassDeclarationCondition :IClassDeclarationCondition
    {
        public bool CheckCondition(IClassDeclaration classDeclaration)
        {
            return true;
        }
    }
}
