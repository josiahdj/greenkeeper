﻿using System;
using Greenkeeper.DeclarationSorting;
using JetBrains.ReSharper.Daemon;
using JetBrains.ReSharper.Psi.CSharp;
using JetBrains.ReSharper.Psi.CSharp.Tree;

[assembly:  RegisterConfigurableSeverity(
            DeclarationSorterHighlighting.SeverityId,
            null,
            HighlightingGroupIds.CodeSmell,
            DeclarationSorterHighlighting.Title,
            DeclarationSorterHighlighting.Description,
            Severity.WARNING,
            false)]

namespace Greenkeeper.DeclarationSorting
{
    [ConfigurableSeverityHighlighting(SeverityId, CSharpLanguage.Name, OverlapResolve = OverlapResolveKind.WARNING)]
    public class DeclarationSorterHighlighting : IHighlighting
    {
        private readonly IClassDeclaration _ClassDeclaration;
        private readonly IDeclarationSorter _DeclarationSorter;
        private readonly IAdditionalExecutor _AdditionalExecutor;
        public const string SeverityId = "DeclarationNotSorted";
        public const string Title = "Declarations not sorted";
        public const string Description = "Declarations are not sorted correctly.";
        
        public DeclarationSorterHighlighting(IClassDeclaration classDeclaration, IDeclarationSorter declarationSorter, IAdditionalExecutor additionalExecutor)
        {
            if (classDeclaration == null) throw new ArgumentNullException("classDeclaration");
            if (declarationSorter == null) throw new ArgumentNullException("declarationSorter");
            if (additionalExecutor == null) throw new ArgumentNullException("additionalExecutor");

            _ClassDeclaration = classDeclaration;
            _DeclarationSorter = declarationSorter;
            _AdditionalExecutor = additionalExecutor;
        }

        public string ToolTip
        {
            get { return Description; }
        }

        public string ErrorStripeToolTip
        {
            get { return ToolTip; }
        }

        public int NavigationOffsetPatch
        {
            get { return 0; }
        }

        public bool IsValid()
        {
            return true;
        }

        public IClassDeclaration GetClassDeclaration()
        {
            return _ClassDeclaration;
        }

        public IDeclarationSorter GetDeclarationSorter()
        {
            return _DeclarationSorter;
        }

        public IAdditionalExecutor GetAdditionalExecuter()
        {
            return _AdditionalExecutor;
        }
    }
}

