﻿using System;
using System.Collections.Generic;

namespace Greenkeeper.DeclarationSorting
{
    public class ConditionalDeclarationComparer : IDeclarationComparer
    {
        private readonly IDeclarationComparer _DeclarationComparer;
        private readonly IDeclarationCondition _DeclarationCondition;

        public ConditionalDeclarationComparer(IDeclarationComparer declarationComparer,
                                                        IDeclarationCondition declarationCondition)
        {
            if (declarationComparer == null) throw new ArgumentNullException("declarationComparer");
            if (declarationCondition == null) throw new ArgumentNullException("declarationCondition");
            _DeclarationComparer = declarationComparer;
            _DeclarationCondition = declarationCondition;
        }

        public void InitWithDeclarationOrderElement(IList<DeclarationOrderElement> declarations)
        {
            _DeclarationComparer.InitWithDeclarationOrderElement(declarations);
        }

        public bool NeedResort()
        {
            return _DeclarationComparer.NeedResort();
        }

        public bool NeedSortedInitWithDeclarationOrderElements()
        {
            return _DeclarationComparer.NeedSortedInitWithDeclarationOrderElements();
        }

        public int Compare(DeclarationOrderElement x, DeclarationOrderElement y)
        {
            var conditionX = CheckCondition(x);
            var conditionY = CheckCondition(y);

            if (conditionX && conditionY)
            {
                return _DeclarationComparer.Compare(x, y);
            }
            
            if (conditionX)
            {
                return -1;
            }

            if (conditionY)
            {
                return 1;
            }

            return 0;
        }

        private bool CheckCondition(DeclarationOrderElement declaration)
        {
            return _DeclarationCondition.CheckCondition(declaration);
        }
    }
}
