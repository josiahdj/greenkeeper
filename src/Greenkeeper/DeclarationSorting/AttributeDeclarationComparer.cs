﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using JetBrains.ReSharper.Psi.CSharp.Tree;

namespace Greenkeeper.DeclarationSorting
{
    public class AttributeDeclarationComparer : ReferenceDeclarationComparer<string>
    {
        public AttributeDeclarationComparer(IList<string> attributeOrder)
            : this(IndexItem<string>.CreateIndexItems(attributeOrder))
        {
        }

        public AttributeDeclarationComparer(IList<IndexItem<string>> attributeOrder)
            : base(GetIndexEnumAsStrings(attributeOrder))
        {
        }
       
        protected override ICollection<string> GetReferencesFrom(DeclarationOrderElement declarationOrderElement)
        {
            var declaration = declarationOrderElement.Declaration;
            var attributesOwner = declaration as IAttributesOwnerDeclaration;
            if (attributesOwner == null)
            {
                return new Collection<string>();
            }
            var extractor = new AttributesExtractor(attributesOwner);
            return extractor.GetAttributes();
        }
    }
}
