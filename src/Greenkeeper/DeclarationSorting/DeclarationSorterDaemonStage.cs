﻿using System.Linq;
using JetBrains.Application.Settings;
using JetBrains.DocumentModel;
using JetBrains.ReSharper.Daemon;
using JetBrains.ReSharper.Psi;
using JetBrains.ReSharper.Psi.CSharp;
using JetBrains.ReSharper.Psi.Files;
using JetBrains.ReSharper.Psi.Tree;
using NLog;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Greenkeeper.DeclarationSorting
{
    [DaemonStage(StagesBefore = new[] { typeof(LanguageSpecificDaemonStage) })]
    public class DeclarationSorterDaemonStage : IDaemonStage
    {
        private readonly Logger _Logger;

        public DeclarationSorterDaemonStage()
        {
            _Logger = LogManager.GetCurrentClassLogger();
        }

        public ErrorStripeRequest NeedsErrorStripe(IPsiSourceFile sourceFile, IContextBoundSettingsStore settingsStore)
        {
            return ErrorStripeRequest.STRIPE_AND_ERRORS;
        }

        public IEnumerable<IDaemonStageProcess> CreateProcess(IDaemonProcess process, IContextBoundSettingsStore settings,
                                                 DaemonProcessKind processKind)
        {

            return ExceptionLogger.Execute(_Logger, () => GetExecuteMessage(process), () => CreateDaemonStageProcess(process, settings));
        }

        private static string GetExecuteMessage(IDaemonProcess process)
        {
            var psiFile = PsiFileFinder.FindCSharpPsiFile(process);
            if (psiFile == null)
            {
                return string.Empty;
            }

            var name = string.Empty;
            var sourceFile = psiFile.GetSourceFile();
            if (sourceFile != null)
            {
                name = sourceFile.Name;
            }
            return "ExecuteParameterNullCheckDaemonStage on " + name;
        }
        
        private IEnumerable<IDaemonStageProcess> CreateDaemonStageProcess(IDaemonProcess process, IContextBoundSettingsStore settings)
        {
            var psiFile = PsiFileFinder.FindCSharpPsiFile(process);
            if (psiFile == null)
            {
                return Enumerable.Empty<IDaemonStageProcess>();
            }

            return new Collection<IDaemonStageProcess>{ new DeclarationSorterDaemonProcess(process, settings)};
        }
    }
}
