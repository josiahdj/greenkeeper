using JetBrains.ReSharper.Psi.CSharp.Tree;

namespace Greenkeeper.DeclarationSorting
{
    public interface IAdditionalExecutor
    {
        void InitWithTreeNodeWriter(TreeNodeWriter treeNodeWriter);
        void BeforeSort(IClassDeclaration classDeclaration);
        void AfterSort(IClassDeclaration classDeclaration);
    }
}
