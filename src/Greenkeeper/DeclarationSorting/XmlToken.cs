﻿namespace Greenkeeper.DeclarationSorting
{
    public static class XmlToken
    {
        public const string XmlCommentOutOpen = "<!--";
        public const string XmlCommentOutClosed = "-->";
        public const string XmlCommentOutOpenFake = "<!-";
        public const string XmlCommentOutClosedFake = "->";
        public const string XmlFileHeader = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
    }
}
