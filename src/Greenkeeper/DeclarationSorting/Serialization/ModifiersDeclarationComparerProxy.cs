﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Greenkeeper.DeclarationSorting.Serialization
{
    [XmlType("ModifiersDeclarationComparer")]
    public class ModifiersDeclarationComparerProxy : DeclarationComparerProxyBase, IItemConverter<ModifierName>
    {
        [XmlAttribute("ModifierNameOrder")]
        public string ModifierNameOrder { get; set; }
        
        public override IDeclarationComparer CreateDeclarationComparer()
        {
            var splittedModifierNameOrder = SplitModifierNameOrder();
            return new ModifiersDeclarationComparer(splittedModifierNameOrder);
        }

        private IList<IndexItem<ModifierName>> SplitModifierNameOrder()
        {
            var splitter = new IndexItemSplitter<ModifierName>(ModifierNameOrder, this);
            return splitter.ExecuteSplitting();
        }

        public ModifierName Convert(string value)
        {
            return (ModifierName)Enum.Parse(typeof(ModifierName), value, true);
        }
    }
}
