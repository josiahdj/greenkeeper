﻿using System.Collections.Generic;

namespace Greenkeeper.DeclarationSorting.Serialization
{
    public class AttributeSplitter : IItemConverter<string>
    {
        public IList<IndexItem<string>> Split(string attributes)
        {
            var splitter = new IndexItemSplitter<string>(attributes, this);
            return splitter.ExecuteSplitting();
        }

        public string Convert(string value)
        {
            return value;
        }
    }
}
