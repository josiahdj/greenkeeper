﻿using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using JetBrains.ReSharper.Psi;

namespace Greenkeeper.DeclarationSorting.Serialization
{
    public class AccessRightsSplitter : IItemConverter<AccessRights>
    {
        public AccessRights Convert([NotNull] string value)
        {
            if (value == null) throw new ArgumentNullException("value");
            return (AccessRights)Enum.Parse(typeof(AccessRights), value, true);
        }

        public IList<AccessRights> SplitFlat([NotNull] string accessRights)
        {
            if (accessRights == null) throw new ArgumentNullException("accessRights");

            var indexItems = Split(accessRights);
            return indexItems.Select(i => i.Item).ToList();
        }

        public IList<IndexItem<AccessRights>> Split([NotNull] string accessRights)
        {
            if (accessRights == null) throw new ArgumentNullException("accessRights");
            var splitter = new IndexItemSplitter<AccessRights>(accessRights, this);
            return splitter.ExecuteSplitting();
        }
    }
}
