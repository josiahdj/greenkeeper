﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Greenkeeper.DeclarationSorting.Serialization
{
    [XmlType("AttributeDeclarationComparer")]
    public class AttributeDeclarationComparerProxy : DeclarationComparerProxyBase
    {
        [XmlAttribute("AttributeOrder")]
        public string AttributeOrder { get; set; }
        
        public override IDeclarationComparer CreateDeclarationComparer()
        {
            var splittedAttributes = SplitAttributeOrder();
            return new AttributeDeclarationComparer(splittedAttributes);
        }

        private IList<IndexItem<string>> SplitAttributeOrder()
        {
            return new AttributeSplitter().Split(AttributeOrder);
        }
    }
}
