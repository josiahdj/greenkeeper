﻿using System.Xml.Serialization;

namespace Greenkeeper.DeclarationSorting.Serialization
{
    [XmlType("BrokenRegionRemoverExecutor")]
    public class BrokenRegionRemoverExecutorProxy : AdditionalExecutorProxyBase
    {
        public override IAdditionalExecutor CreateAdditionalExecutor()
        {
            return new BrokenRegionRemoverExecutor();
        }
    }
}
