﻿using System.Linq;
using System.Xml.Serialization;

namespace Greenkeeper.DeclarationSorting.Serialization
{
    [XmlType("TypeDeclarationCondition")]
    public class TypeDeclarationConditionProxy : DeclarationConditionProxyBase
    {
        [XmlAttribute("Types")]
        public string Types { get; set; }

        public override IDeclarationCondition CreateDeclarationCondition()
        {
            var splitter = new TypeNameSplitter();
            var typeOrder = splitter.Split(Types).Select(i => i.Item).ToList();
            return new TypeDeclarationCondition(typeOrder);
        }
    }
}
