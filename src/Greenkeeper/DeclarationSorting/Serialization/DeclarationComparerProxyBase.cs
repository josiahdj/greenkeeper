﻿
namespace Greenkeeper.DeclarationSorting.Serialization
{
    public abstract class DeclarationComparerProxyBase
    {
        public abstract IDeclarationComparer CreateDeclarationComparer();
    }
}
