﻿using System.Xml.Serialization;

namespace Greenkeeper.DeclarationSorting.Serialization
{
    [XmlType("ConditionalDeclarationSorter")]
    public class ConditionalDeclarationSorterProxy
    {
        [XmlElement("ClassDeclarationCondition")]
        public XmlSerializable<ClassDeclarationConditionProxyBase> ClassDeclarationCondition { get; set; }

        [XmlElement("DeclarationSorter")]
        public XmlSerializable<DeclarationSorterProxyBase> DeclarationSorter { get; set; }

        public ConditionalDeclarationSorter CreateConditionalDeclarationSorter()
        {
            return new ConditionalDeclarationSorter(ClassDeclarationCondition.Serialized.CreateClassDeclarationCondition(), DeclarationSorter.Serialized.CreateDeclarationSorter());
        }
    }
}
