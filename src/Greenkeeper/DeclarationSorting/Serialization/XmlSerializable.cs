﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace Greenkeeper.DeclarationSorting.Serialization
{
    /// <summary>
    /// Provides the ability to serialize an abstract class.
    /// </summary>
    public class XmlSerializable<T> : IXmlSerializable
    {
        private const string _TypeAttribute = "type";
        private readonly string _ErrorMessagePrefix;
        public T Serialized { get; set; }
       
        public XmlSerializable()
        {
            _ErrorMessagePrefix = "Unable to Read Xml Serialized for Abstract Type '" + typeof (T).Name + "'";
        }
    
        public XmlSerializable(T serialized)
            :this()
        {
            Serialized = serialized;
        }
        
        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null; // this is fine as schema is unknown.
        }

        public void ReadXml(XmlReader reader)
        {
            var typeAttribute = GetTypeAttribute(reader);
            var type = GetType(typeAttribute);
            var xmlData = ReadXmlElement(reader);
            xmlData = SurroundWithTypeElement(xmlData, type);
            Serialized = GetSerialized(xmlData, type);
        }

        private string GetTypeAttribute(XmlReader reader)
        {
            string typeAttrib = reader.GetAttribute(_TypeAttribute);

            if (typeAttrib == null)
                throw new ArgumentNullException(_ErrorMessagePrefix
                                                + " because no '"+_TypeAttribute+"' attribute was specified in the XML.");

            return typeAttrib;
        }

        private Type GetType(string typeAttrib)
        {
            var types = GetTypeWithXmlTypeValue(typeAttrib);

            if (types.Count > 1)
                throw new InvalidCastException(_ErrorMessagePrefix +
                                               "because multiple types specified the same name in XmlTypeAttribute.");

            if (types.Count == 0)
                throw new InvalidCastException(_ErrorMessagePrefix +
                                               " because the type specified in the XML was not found.");

            var type = types.First();

            if (!type.IsSubclassOf(typeof (T)))
                throw new InvalidCastException(_ErrorMessagePrefix +
                                               " because the Type specified in the XML differs ('" + type.Name + "').");

            return type;
        }

        private ICollection<Type> GetTypeWithXmlTypeValue(string xmlTypeName)
        {
            var types = new Collection<Type>();
            var assembly = Assembly.GetExecutingAssembly();
            foreach (Type type in assembly.GetTypes())
            {
                var attributes = GetXmlTypeAttribute(type);
                if (IsXmlTypeAttribute(xmlTypeName,attributes))
                {
                    types.Add(type);
                }
            }
            return types;
        }

        private ICollection<XmlTypeAttribute> GetXmlTypeAttribute(Type type)
        {
            return type.GetCustomAttributes(typeof(XmlTypeAttribute), false)
                                        .Cast<XmlTypeAttribute>()
                                        .ToList();
        }

        private bool IsXmlTypeAttribute(string xmlTypeName,ICollection<XmlTypeAttribute> attributes)
        {
            return attributes.Count() == 1
                   && attributes.Any(a => String.Equals(a.TypeName, xmlTypeName));
        }

        private string ReadXmlElement(XmlReader xmlReader)
        {
            return xmlReader.ReadOuterXml();
        }

        private string SurroundWithTypeElement(string xmlData, Type type)
        {
            var document = XDocument.Parse(xmlData);
            var typeName = GetXmlTypeAttributeName(type);
            if (document.Root == null)
            {
                throw new NullReferenceException("document.Root");
            }
            document.Root.Name = typeName;
            return document.ToString();
        }

        private T GetSerialized(string xmlData, Type type)
        {
            T serialized;
            using (var reader = new StringReader(xmlData))
            {
                serialized = (T)new XmlSerializer(type).Deserialize(reader);
            }
            return serialized;
        }
        
        public void WriteXml(XmlWriter writer)
        {
           throw new NotImplementedException();
        }

        private string GetXmlTypeAttributeName(Type type)
        {
            var attributes = GetXmlTypeAttribute(type);

            if (attributes.Count > 1)
                throw new InvalidCastException(_ErrorMessagePrefix +
                                               "because multiple XmlTypeAttribute in one class.");

            if (attributes.Count == 0)
                throw new InvalidCastException(_ErrorMessagePrefix +
                                               " because XmlTypeAttribute in the class was not found.");

            return attributes.First().TypeName;
        }
    }
}