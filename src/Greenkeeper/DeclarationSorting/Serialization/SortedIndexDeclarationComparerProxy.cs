﻿using System.Xml.Serialization;

namespace Greenkeeper.DeclarationSorting.Serialization
{
    [XmlType("SortedIndexDeclarationComparer")]
    public class SortedIndexDeclarationComparerProxy : DeclarationComparerProxyBase
    {
        public override IDeclarationComparer CreateDeclarationComparer()
        {
            return new SortedIndexDeclarationComparer();
        }
    }
}
