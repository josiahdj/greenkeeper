﻿using System.Xml.Serialization;

namespace Greenkeeper.DeclarationSorting.Serialization
{
    [XmlType("DeclarationSorter")]
    public class DeclarationSorterProxy : DeclarationSorterProxyBase
    {
        [XmlElement("DeclarationComparer")]
        public XmlSerializable<DeclarationComparerProxyBase> DeclarationComparerProxy { get; set; }

        public override IDeclarationSorter CreateDeclarationSorter()
        {
            var declarationComparer = DeclarationComparerProxy.Serialized.CreateDeclarationComparer();
            return new DeclarationSorter(declarationComparer);  
        }
    }
}
