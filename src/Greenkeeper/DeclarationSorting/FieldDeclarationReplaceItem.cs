﻿using JetBrains.ReSharper.Psi.CSharp.Tree;
using JetBrains.ReSharper.Psi.Tree;

namespace Greenkeeper.DeclarationSorting
{
    class FieldDeclarationReplaceItem : IDeclarationReplaceItem
    {
        public bool MustReplace(IDeclaration declaration)
        {
            return declaration is IFieldDeclaration && declaration.Parent is IMultipleFieldDeclaration;
        }

        public ITreeNode GetReplacement(IDeclaration declaration)
        {
            return declaration.Parent;
        }
    }
}
