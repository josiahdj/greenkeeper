﻿using System;
using System.Collections.Generic;
using JetBrains.ReSharper.Psi.CSharp.Tree;

namespace Greenkeeper.DeclarationSorting
{
    public class MultiConditionalDeclarationSorter : IDeclarationSorter
    {
        private readonly ICollection<ConditionalDeclarationSorter> _ConditionalDeclarationSorters;
        private IClassDeclaration _ClassDeclaration;


        public MultiConditionalDeclarationSorter(ICollection<ConditionalDeclarationSorter> conditionalDeclarationSorters)
        {
            if (conditionalDeclarationSorters == null) throw new ArgumentNullException("conditionalDeclarationSorters");
            _ConditionalDeclarationSorters = conditionalDeclarationSorters;
        }

        public void InitWithClassDeclaration(IClassDeclaration classDeclaration)
        {
            if (classDeclaration == null) throw new ArgumentNullException("classDeclaration");
            _ClassDeclaration = classDeclaration;
        }

        public bool IsClassSorted()
        {
            var firstDeclarationSorter = GetFirstMatchingDeclarationSorterAndInit();
            return firstDeclarationSorter.IsClassSorted();
        }

        public ICollection<SortedDeclaration> GetSortedDeclaration()
        {
            var firstDeclarationSorter = GetFirstMatchingDeclarationSorterAndInit();
            return firstDeclarationSorter.GetSortedDeclaration();
        }

        private IDeclarationSorter GetFirstMatchingDeclarationSorterAndInit()
        {
            var firstDeclarationSorter = GetFirstMatchingDeclarationSorter();
            if (firstDeclarationSorter == null)
            {
                throw new NullReferenceException("firstDeclarationSorter");
            }
            firstDeclarationSorter.InitWithClassDeclaration(_ClassDeclaration);
            return firstDeclarationSorter;
        }

        private IDeclarationSorter GetFirstMatchingDeclarationSorter()
        {
            foreach (var conditionalDeclarationSorter in _ConditionalDeclarationSorters)
            {
                if (conditionalDeclarationSorter.MatchCondition(_ClassDeclaration))
                {
                    return conditionalDeclarationSorter.GetDeclarationSorter();
                }
            }
            return new NoDeclarationSorter();
        }
    }
}
