﻿using System.Text;
using JetBrains.Annotations;
using JetBrains.Application.Progress;
using JetBrains.DocumentModel;
using JetBrains.ProjectModel;
using JetBrains.ReSharper.Psi;
using JetBrains.ReSharper.Psi.CSharp;
using JetBrains.ReSharper.Psi.CSharp.Tree;
using JetBrains.ReSharper.Psi.Files;
using JetBrains.ReSharper.Psi.Resolve;
using JetBrains.ReSharper.Psi.Search;
using JetBrains.ReSharper.Psi.Tree;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using NLog;

namespace Greenkeeper.DeclarationSorting
{
    class DeclarationUsageInClassFinder
    {
        private readonly ICollection<IDeclaration> _Declarations;
        private IClassDeclaration _ClassDeclaration;
        private IPsiServices _PsiServices;
        private ICollection<IDeclaredElement> _DeclaredElements;
        private readonly Logger _Logger;
        
        public DeclarationUsageInClassFinder(ICollection<IDeclaration> declarations)
        {
            if (declarations == null) throw new ArgumentNullException("declarations");
            _Declarations = declarations;
            _Logger = LogManager.GetCurrentClassLogger();
        }

        public ICollection<DeclarationUsage> Find()
        {
            if (_Declarations.Count > 0)
            {
                SetPsiServices();
                SetDeclaredElements();
                SetClassDeclaration();
                return FindUsages();
            }
            return new Collection<DeclarationUsage>();
        }

        private void SetPsiServices()
        {
            _PsiServices = _Declarations.First().GetPsiServices();
        }

        private void SetDeclaredElements()
        {
            _DeclaredElements = _Declarations.Select(d => d.DeclaredElement).ToList();
        }

        private void SetClassDeclaration()
        {
            _ClassDeclaration = GetClassDeclarationFromTreeNode(_Declarations.First());
        }

        private IClassDeclaration GetClassDeclarationFromTreeNode(ITreeNode treeNode)
        {
            return treeNode.PathToRoot().OfType<IClassDeclaration>().Single();
        }

        private ICollection<DeclarationUsage> FindUsages()
        {
            var declarationUsages = new Collection<DeclarationUsage>();
            var declarationUsageFindResults = FindReferences();
            foreach (var usage in declarationUsageFindResults)
            {
                var declarationUsage = CreateDeclarationUsage(usage);
                if (declarationUsage != null && IsMethodFromClass(declarationUsage))
                {
                    declarationUsages.Add(declarationUsage);
                }
            }
            return declarationUsages;
        }

        private ICollection<DeclarationUsageFindResult> FindReferences()
        {
            var consumer = new DeclarationUsageFindConsumer();
            _PsiServices.Finder.FindReferences(_DeclaredElements, CreateSearchInClassDomain()
                                               , consumer, NullProgressIndicator.Instance);

            var declarationUsageFindResults = consumer.GetFindResults();
            return declarationUsageFindResults;
        }

        private ISearchDomain CreateSearchInClassDomain()
        {
            var searchDomainFactory = GetSearchDomainFactory();
            return searchDomainFactory.CreateSearchDomain(_ClassDeclaration);
        }

        private SearchDomainFactory GetSearchDomainFactory()
        {
            return _ClassDeclaration.GetSolution().GetComponent<SearchDomainFactory>();
        }

        private bool IsMethodFromClass(DeclarationUsage declarationUsage)
        {
            var methodsClass = GetClassDeclarationFromTreeNode(declarationUsage.UsedBy);
            return methodsClass == _ClassDeclaration;
        }

        private DeclarationUsage CreateDeclarationUsage(DeclarationUsageFindResult findResult)
        {
            var treeTextRange = GetElementPosition(findResult.Reference);
            var usedBy = GetContainer(findResult.Reference, treeTextRange);
            if (usedBy == null)
            {
                return null;
            }
            return new DeclarationUsage(findResult.DeclaredElement, usedBy, treeTextRange);
        }

        private TreeTextRange GetElementPosition(IReference reference)
        {
            return reference.GetTreeTextRange();
        }

        private IDeclaration GetContainer(IReference reference, TreeTextRange treeTextRange)
        {
            var file = GetFile(reference);
            var node = GetNode(file, treeTextRange);
            var nodes = node.PathToRoot()
                                   .Where(IsValidContainer);
                       
            var declarations = nodes.Cast<IDeclaration>().ToList();
            if (declarations.Count != 1)
            {
                LogInvalidContainingTree(node, treeTextRange);
            }
            return declarations.SingleOrDefault();
        }

        private bool IsValidContainer(ITreeNode treeNode)
        {
            return IsFunctionButNoAccessor(treeNode) ||
                   IsProperty(treeNode) ||
                   IsFieldDeclaration(treeNode) ||
                   IsIndexerDeclaration(treeNode) ||
                   IsConstantDeclaration(treeNode);
        }

        private bool IsConstantDeclaration(ITreeNode treeNode)
        {
            return treeNode is IConstantDeclaration;
        }

        private bool IsIndexerDeclaration(ITreeNode treeNode)
        {
            return treeNode is IIndexerDeclaration;
        }

        private bool IsFieldDeclaration(ITreeNode treeNode)
        {
            return treeNode is IFieldDeclaration;
        }

        private void LogInvalidContainingTree( ITreeNode node,TreeTextRange treeTextRange)
        {
            var stringBuilder = new  StringBuilder();
            stringBuilder.Append("GetContainer() can not detect an valid container through the tree for the following usage: ");
            stringBuilder.Append("->");
            stringBuilder.Append(node.GetText());
            stringBuilder.Append("(Position:");
            stringBuilder.Append(treeTextRange);
            stringBuilder.Append(")");
            _Logger.Log(LogLevel.Warn, stringBuilder);
        }

        private static bool IsProperty(ITreeNode node)
        {
            return node is IPropertyDeclaration;
        }

        private static bool IsFunctionButNoAccessor(ITreeNode node)
        {
            return node is ICSharpFunctionDeclaration && !(node is IAccessorDeclaration);
        }

        private ICSharpFile GetFile(IReference reference)
        {
            var sourceFile = reference.GetTreeNode().GetSourceFile();
            if (sourceFile == null) throw new NullReferenceException("sourceFile");
            return sourceFile.GetPsiFiles<CSharpLanguage>().OfType<ICSharpFile>().Single();
        }
        
        private ITreeNode GetNode(ICSharpFile file, TreeTextRange treeTextRange)
        {
            var node = file.FindNodeAt(treeTextRange);
            if (node == null)
            {
                throw new NullReferenceException("node");
            }
            return node;
        }
    }
}
