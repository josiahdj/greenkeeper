﻿using System;
using System.Collections.Generic;
using JetBrains.ReSharper.Psi.CSharp.Tree;

namespace Greenkeeper.DeclarationSorting
{
    public class MultiAdditionalExecutor : IAdditionalExecutor
    {
        private readonly ICollection<IAdditionalExecutor> _AdditionalExecuters;

        public MultiAdditionalExecutor(ICollection<IAdditionalExecutor> additionalExecuters )
        {
            if (additionalExecuters == null) throw new ArgumentNullException("additionalExecuters");
            _AdditionalExecuters = additionalExecuters;
        }
        
        public void InitWithTreeNodeWriter(TreeNodeWriter treeNodeWriter)
        {
            ExecuteForAll(i => i.InitWithTreeNodeWriter(treeNodeWriter));
        }

        public void BeforeSort(IClassDeclaration classDeclaration)
        {
            ExecuteForAll(i => i.BeforeSort(classDeclaration));
        }

        public void AfterSort(IClassDeclaration classDeclaration)
        {
            ExecuteForAll(i => i.AfterSort(classDeclaration));
        }

        private void ExecuteForAll(Action<IAdditionalExecutor> interfaceAction)
        {
            foreach (var additionalExecuter in _AdditionalExecuters)
            {
                interfaceAction(additionalExecuter);
            }
        }

    }
}
