﻿using System;
using System.Collections.Generic;
using JetBrains.ReSharper.Psi.CSharp.Tree;

namespace Greenkeeper.DeclarationSorting
{
    public class NoDeclarationSorter : IDeclarationSorter
    {
        private IClassDeclaration _ClassDeclaration;

        public void InitWithClassDeclaration(IClassDeclaration classDeclaration)
        {
            if (classDeclaration == null) throw new ArgumentNullException("classDeclaration");
            _ClassDeclaration = classDeclaration;
        }

        public bool IsClassSorted()
        {
            return true;
        }

        public ICollection<SortedDeclaration> GetSortedDeclaration()
        {
            return SortedDeclarationCreator.CreateUnsorted(_ClassDeclaration); 
        }
    }
}
