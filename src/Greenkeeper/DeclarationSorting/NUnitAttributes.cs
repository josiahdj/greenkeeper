﻿namespace Greenkeeper.DeclarationSorting
{
    public class NUnitAttributes
    {
        public const string Class = "TestFixture";
        public const string ClassSetUp = "TestFixtureSetUp";
        public const string ClassTearDown = "TestFixtureTearDown";
        public const string SetUp = "SetUp";
        public const string TearDown = "TearDown";
        public const string Test = "Test";
        public const string TestCase = "TestCase";
    }
}
