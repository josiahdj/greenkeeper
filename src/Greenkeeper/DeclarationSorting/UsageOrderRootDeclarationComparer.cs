﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Greenkeeper.DeclarationSorting
{
    public class UsageOrderRootDeclarationComparer : DefaultDeclarationComparer
    {
        private IList<DeclarationOrderElement> _DeclarationOrderElements;
        private ICollection<DeclarationUsage> _DeclarationUsages;

        public override void InitWithDeclarationOrderElement(IList<DeclarationOrderElement> declarations)
        {
             if (declarations == null) throw new ArgumentNullException("declarations");
            _DeclarationOrderElements = declarations;
            FindDeclarationUsages();
        }

        private void FindDeclarationUsages()
        {
            var declarations = _DeclarationOrderElements.Select(d => d.Declaration).ToList();
            var finder = new DeclarationUsageInClassFinder(declarations);
            _DeclarationUsages = finder.Find();
        }

        public override int Compare(DeclarationOrderElement x, DeclarationOrderElement y)
        {
            return GetRootIndex(x).CompareTo(GetRootIndex(y));
        }
        
        private int GetRootIndex(DeclarationOrderElement declarationOrderElement)
        {
            return IsRoot(declarationOrderElement) ? 0 : 1;
        }

        private bool IsRoot(DeclarationOrderElement declarationOrderElement)
        {
            return !IsCalled(declarationOrderElement);
        }
        
        private bool IsCalled(DeclarationOrderElement declarationOrderElement)
        {
            return _DeclarationUsages.Any(u => Equals(u.DeclaredElement, declarationOrderElement.Declaration.DeclaredElement));
        }
    }
}
