﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using JetBrains.ReSharper.Psi;
using JetBrains.ReSharper.Psi.CSharp.Tree;
using JetBrains.ReSharper.Psi.Tree;

namespace Greenkeeper.DeclarationSorting
{
    public class RegionDeclarationRange
    {
        public IStartRegion Region { get; private set; }
        public TreeTextRange TreeTextRange { get; private set; }
        public ICollection<IDeclaration> ChildDeclarations { get; private set; }

        public ICollection<IDeclaration> DeclarationsInTreeTextRange { get; private set; }
        public ICollection<IStartRegion> RegionsInTreeTextRange { get; private set; }
        
        public RegionDeclarationRange(IStartRegion region, TreeTextRange treeTextRange)
        {
            if (region == null) throw new ArgumentNullException("region");
            Region = region;
            TreeTextRange = treeTextRange;
            ChildDeclarations = new Collection<IDeclaration>();
        }

        public void SetDeclarationsInTreeTextRange(ICollection<IDeclaration> declarations)
        {
            if (declarations == null) throw new ArgumentNullException("declarations");
            DeclarationsInTreeTextRange = declarations;
        }

        public void SetRegionsInTreeTextRange(ICollection<IStartRegion> childRegions)
        {
            if (childRegions == null) throw new ArgumentNullException("childRegions");
            RegionsInTreeTextRange = childRegions;
        }


    }
}
