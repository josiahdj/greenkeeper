﻿using System;
using Greenkeeper.DeclarationSorting.Serialization;
using JetBrains.Application.Settings;

namespace Greenkeeper.DeclarationSorting
{
    public class DeclarationSorterSettingsLoader
    {
        private readonly IContextBoundSettingsStore _SettingsStore;
        private DeclarationSorterSettings _DeclarationSorterSettings;

        public DeclarationSorterSettingsLoader(IContextBoundSettingsStore settingsStore)
        {
            if (settingsStore == null) throw new ArgumentNullException("settingsStore");
            _SettingsStore = settingsStore;
        }

        public DeclarationSorterSettings Load()
        {
            LoadSettingsFromStore();
            CheckSettingsAndOverrideWithDefaultIfInvalid();
            return _DeclarationSorterSettings;
        }

        private void LoadSettingsFromStore()
        {
            _DeclarationSorterSettings = _SettingsStore.GetKey<DeclarationSorterSettings>(SettingsOptimization.OptimizeDefault);
        }

        private void CheckSettingsAndOverrideWithDefaultIfInvalid()
        {
            if (!IsSerializedDefinitionValid())
            {
                OverrideWithDefault();
            }
        }

        private bool IsSerializedDefinitionValid()
        {
            var checker = new DeclarationSorterSettingsChecker();
            return checker.IsSerializedDefinitionValid(_DeclarationSorterSettings);
        }

        private void OverrideWithDefault()
        {
            if (ExistsXml())
            {
                CommentOutExistingXml();
            }
            AddDefault();
        }

        private bool ExistsXml()
        {
            return !String.IsNullOrEmpty(_DeclarationSorterSettings.SerializedDeclarationSorterDefinition);
        }

        private void CommentOutExistingXml()
        {
            _DeclarationSorterSettings.SerializedDeclarationSorterDefinition = XmlCommentOut(_DeclarationSorterSettings.SerializedDeclarationSorterDefinition)
                                                                               + Environment.NewLine;
        }

        private string XmlCommentOut(string innerXml)
        {
            innerXml = ReplaceXmlCommentOutWithFake(innerXml);
            return XmlToken.XmlCommentOutOpen + innerXml + XmlToken.XmlCommentOutClosed;
        }

        private string ReplaceXmlCommentOutWithFake(string innerXml)
        {
            return innerXml.Replace(XmlToken.XmlCommentOutOpen, XmlToken.XmlCommentOutOpenFake)
                           .Replace(XmlToken.XmlCommentOutClosed, XmlToken.XmlCommentOutClosedFake);
        }

        private void AddDefault()
        {
            _DeclarationSorterSettings.SerializedDeclarationSorterDefinition = XmlToken.XmlFileHeader
                                                                               + Environment.NewLine
                                                                               + _DeclarationSorterSettings.SerializedDeclarationSorterDefinition
                                                                               + GetDefault();
        }

        private string GetDefault()
        {
            return _DeclarationSorterSettings.GetDefaultSerializedDeclarationSorterDefinition();
        }
    }
}
