﻿using JetBrains.ReSharper.Psi.CSharp.Tree;

namespace Greenkeeper.DeclarationSorting
{
    public interface IClassDeclarationCondition
    {
        bool CheckCondition(IClassDeclaration classDeclaration);
    }
}
