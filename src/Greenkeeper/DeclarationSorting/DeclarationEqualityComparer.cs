﻿using System.Collections.Generic;
using JetBrains.ReSharper.Psi.Tree;

namespace Greenkeeper.DeclarationSorting
{
    public class DeclarationEqualityComparer : IEqualityComparer<IDeclaration>
    {
        private static IEqualityComparer<IDeclaration> _declarationComparer;
        public static IEqualityComparer<IDeclaration> DeclarationComparer
        {
            get { return _declarationComparer ?? (_declarationComparer = new DeclarationEqualityComparer()); }
        }

        public bool Equals(IDeclaration declarationA, IDeclaration declarationB)
        {
            if (ReferenceEquals(declarationA, declarationB)) return true;
            if (ReferenceEquals(declarationA, null)) return false;
            if (ReferenceEquals(declarationB, null)) return false;
            return string.Equals(declarationA.DeclaredName, declarationB.DeclaredName) && string.Equals(declarationA.GetText(), declarationB.GetText());
        }

        public int GetHashCode(IDeclaration obj)
        {
            unchecked
            {
                return ((obj.DeclaredName != null ? obj.DeclaredName.GetHashCode() : 0) * 397) ^ (obj.GetText() != null ? obj.GetText().GetHashCode() : 0);
            }
        }
    }

   
}
