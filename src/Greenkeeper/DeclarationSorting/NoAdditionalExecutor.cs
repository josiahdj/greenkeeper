﻿using JetBrains.ReSharper.Psi.CSharp.Tree;

namespace Greenkeeper.DeclarationSorting
{
    public class NoAdditionalExecutor : IAdditionalExecutor
    {
        public void InitWithTreeNodeWriter(TreeNodeWriter treeNodeWriter)
        {
        }

        public void BeforeSort(IClassDeclaration classDeclaration)
        {
        }

        public void AfterSort(IClassDeclaration classDeclaration)
        {
        }
    }
}
