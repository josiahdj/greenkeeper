﻿using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.ReSharper.Psi.CSharp;
using JetBrains.ReSharper.Psi.CSharp.Tree;
using JetBrains.ReSharper.Psi.Tree;

namespace Greenkeeper.DeclarationSorting
{
    public class RegionKeeper
    {
        private readonly ICollection<RegionDeclarationRange> _RegionDeclarationRangesBeforeModification;
        private readonly TreeNodeWriter _TreeNodeWriter;
        private readonly IClassDeclaration _ClassDeclarationAfterSort;
        private readonly Stack<RegionDeclarationRange> _OpenedRegions;
        private IDeclaration _CurrentDeclaration;
        private RegionDeclarationRange _CurrentRegion;
        private IDeclaration _LastDeclaration;
        private IList<IDeclaration> _Declarations;
        private CSharpElementFactory _CSharpElementFactory;
         
        public RegionKeeper(IClassDeclaration classDeclaration, ICollection<RegionDeclarationRange> regionDeclarationRangesBeforeModification,TreeNodeWriter treeNodeWriter)
        {
            if (classDeclaration == null) throw new ArgumentNullException("classDeclaration");
            if (regionDeclarationRangesBeforeModification == null) throw new ArgumentNullException("regionDeclarationRangesBeforeModification");
            _ClassDeclarationAfterSort = classDeclaration;
            _RegionDeclarationRangesBeforeModification = regionDeclarationRangesBeforeModification;
            _TreeNodeWriter = treeNodeWriter;
            _OpenedRegions = new Stack<RegionDeclarationRange>();
        }

        public void KeepRegions()
        {
            RemoveAllRegions();
            RestoreRegions();
        }

        private void RemoveAllRegions()
        {
            var regionsToRemove = _RegionDeclarationRangesBeforeModification.Select(r => r.Region).ToList();
            var remover = new RegionRemover(_TreeNodeWriter, regionsToRemove);
            remover.Remove();
        }

        private void RestoreRegions()
        {
            _Declarations = new ClassDeclarationExtractor(_ClassDeclarationAfterSort).GetDeclarations();
            SetElementFactory();
            foreach (var declaration in _Declarations)
            {
                _CurrentDeclaration = declaration;
                SetCurrentRegionForCurrentDeclaration();
                WriteRegion();
                _LastDeclaration = _CurrentDeclaration;
            }
            CloseAllLeftRegions();
        }

        private void SetElementFactory()
        {
            _CSharpElementFactory = CSharpElementFactory.GetInstance(_ClassDeclarationAfterSort.GetPsiModule());
        }

        private void CloseAllLeftRegions()
        {
            _CurrentRegion = null;
            _CurrentDeclaration = null;
            CloseAllCloseableRegions();
        }

        private void SetCurrentRegionForCurrentDeclaration()
        {
            foreach (var regionDeclarationRange in _RegionDeclarationRangesBeforeModification)
            {
                if (IsRegionForCurrentDeclaration(regionDeclarationRange))
                {
                    _CurrentRegion = regionDeclarationRange;
                    return;
                }
            }
            _CurrentRegion = null;
        }

        private bool IsRegionForCurrentDeclaration(RegionDeclarationRange regionDeclarationRange)
        {
            foreach (var declarationInRegion in regionDeclarationRange.ChildDeclarations)
            {
                if (DeclarationEqualityComparer.DeclarationComparer.Equals(declarationInRegion, _CurrentDeclaration))
                {
                    return true;
                }
            }
            return false;
        }

        private void WriteRegion()
        {
            CloseAllCloseableRegions();
            if (CanOpenRegion())
            {
                OpenCurrentRegion();
            }
        }

        private void CloseAllCloseableRegions()
        {
            while (CanCloseLastRegion())
            {
                CloseLastRegion();
            }
        }

        private bool ExistsOpenedRegions()
        {
            return _OpenedRegions.Count > 0;
        }

        private bool CanCloseLastRegion()
        {
            return ExistsOpenedRegions() &&
                   !LastRegionContainsCurrentRegion()
                   && !LastRegionIsCurrentRegion();
        }

        private bool LastRegionContainsCurrentRegion()
        {
            var lastRegion = _OpenedRegions.Peek();
            return _CurrentRegion != null && lastRegion.RegionsInTreeTextRange.Any(r => Equals(r, _CurrentRegion.Region));
        }

        private bool LastRegionIsCurrentRegion()
        {
            var lastRegion = _OpenedRegions.Peek();
            return _CurrentRegion != null && Equals(lastRegion.Region, _CurrentRegion.Region);
        }

        private void CloseLastRegion()
        {
            _OpenedRegions.Pop();
            var endRegion = CreateEndRegion();
            var replacedDeclaration = GetReplacedDeclaration(_LastDeclaration);
            _TreeNodeWriter.InsertAfter(replacedDeclaration, endRegion);
        }

        private IEndRegion CreateEndRegion()
        {
            return _CSharpElementFactory.CreateRegionDirective(string.Empty).B;
        }

        private bool CanOpenRegion()
        {
            return _CurrentRegion != null && 
                (!ExistsOpenedRegions() || !LastRegionIsCurrentRegion());
        }
        
        private void OpenCurrentRegion()
        {
            var replacedDeclaration = GetReplacedDeclaration(_CurrentDeclaration);
            _TreeNodeWriter.InsertBefore(replacedDeclaration, _CurrentRegion.Region);
            _OpenedRegions.Push(_CurrentRegion);
        }

        private ITreeNode GetReplacedDeclaration(IDeclaration declaration)
        {
            var replacer = DeclarationReplacer.CreateDefaultDeclarationReplacer();
            return replacer.GetReplacedDeclaration(declaration);
        }
    }
}
