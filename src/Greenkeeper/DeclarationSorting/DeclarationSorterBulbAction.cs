﻿using JetBrains.ProjectModel;
using JetBrains.ReSharper.Intentions.Extensibility;
using JetBrains.ReSharper.Psi.CSharp.Tree;
using JetBrains.TextControl;
using NLog;
using System;

namespace Greenkeeper.DeclarationSorting
{
    public class DeclarationSorterBulbAction : IBulbAction
    {
        private readonly Logger _Logger;
        private readonly IClassDeclaration _ClassDeclaration;
        private readonly IDeclarationSorter _DeclarationSorter;
        private readonly IAdditionalExecutor _AdditionalExecutor;
        public const string DeclarationSorterName = "Sort all declaration in class";
        public const string DeclarationSorterDescription = "Sort all declaration in class based on configuration";

        public DeclarationSorterBulbAction(IClassDeclaration classDeclaration, IDeclarationSorter declarationSorter,IAdditionalExecutor additionalExecutor)
        {
            if (classDeclaration == null) throw new ArgumentNullException("classDeclaration");
            if (declarationSorter == null) throw new ArgumentNullException("declarationSorter");
            if (additionalExecutor == null) throw new ArgumentNullException("additionalExecutor");

            _ClassDeclaration = classDeclaration;
            _DeclarationSorter = declarationSorter;
            _AdditionalExecutor = additionalExecutor;
            _Logger = LogManager.GetCurrentClassLogger();
        }

        private string GetBulbItemLogDescription()
        {
            return "SortBulbItemExecute on " + _ClassDeclaration.DeclaredName;
        }

        public void Execute(ISolution solution, ITextControl textControl)
        {
            ExceptionLogger.Execute(_Logger, GetBulbItemLogDescription, () => ExecuteSortAndLogTrackedTime(solution, textControl));
        }

        private void ExecuteSortAndLogTrackedTime(ISolution solution, ITextControl textControl)
        {
            TimeTrackerLogger.Execute(_Logger, LogLevel.Info, GetBulbItemLogDescription, () => ExecuteSort(solution, textControl));
        }

        private void ExecuteSort(ISolution solution, ITextControl textControl)
        {
            if (solution == null) throw new ArgumentNullException("solution");
            if (textControl == null) throw new ArgumentNullException("textControl");

            var sorter = new DeclarationSortExecuter(_ClassDeclaration, _DeclarationSorter, solution,
                                                     _AdditionalExecutor);
            sorter.ExecuteSortToClass();
        }

        public string Text
        {
            get { return DeclarationSorterDescription; }
        }
    }
}
