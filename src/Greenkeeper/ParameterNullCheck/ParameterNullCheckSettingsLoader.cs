﻿using JetBrains.Application.Settings;
using System;

namespace Greenkeeper.ParameterNullCheck
{
    public class ParameterNullCheckSettingsLoader
    {
        private ParameterNullCheckSettings _ParameterNullCheckSettings;
        private IContextBoundSettingsStore _SettingsStore;

        public ParameterNullCheckSettingsLoader(IContextBoundSettingsStore settingsStore)
        {
            if (settingsStore == null) throw new ArgumentNullException("settingsStore");
            _SettingsStore = settingsStore;
        }

        public ParameterNullCheckSettings Load()
        {
            LoadSettingsFromStore();
            return _ParameterNullCheckSettings;
        }

        private void LoadSettingsFromStore()
        {
            _ParameterNullCheckSettings =
                _SettingsStore.GetKey<ParameterNullCheckSettings>(SettingsOptimization.OptimizeDefault);
        }
    }
}