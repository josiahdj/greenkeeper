﻿using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.ReSharper.Psi.CSharp.Tree;

namespace Greenkeeper.ParameterNullCheck
{
    public class AndCriterion : INullCheckRequiredCriterion
    {
        private readonly ICollection<INullCheckRequiredCriterion> _Criteria;

        public AndCriterion(ICollection<INullCheckRequiredCriterion> criteria)
        {
            if (criteria == null) throw new ArgumentNullException("criteria");
            _Criteria = criteria;
        }

        public bool RequireNullCheck(ICSharpFunctionDeclaration functionDeclaration, ICSharpParameterDeclaration parameterDeclaration)
        {
            if (functionDeclaration == null) throw new ArgumentNullException("functionDeclaration");
            if (parameterDeclaration == null) throw new ArgumentNullException("parameterDeclaration");
            return _Criteria.All(c => c.RequireNullCheck(functionDeclaration, parameterDeclaration));
        }
    }
}
