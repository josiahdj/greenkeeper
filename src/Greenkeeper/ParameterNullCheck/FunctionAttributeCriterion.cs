﻿using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.ReSharper.Psi.CSharp.Tree;

namespace Greenkeeper.ParameterNullCheck
{
    public class FunctionAttributeCriterion : INullCheckRequiredCriterion
    {
        private readonly ParameterNullCheckSettings _Settings;
        private ICollection<string> _NoNullCheckAttributes;

        public FunctionAttributeCriterion(ParameterNullCheckSettings settings)
        {
            if (settings == null) throw new ArgumentNullException("settings");
            _Settings = settings;
        }

        public bool RequireNullCheck(ICSharpFunctionDeclaration functionDeclaration, ICSharpParameterDeclaration parameterDeclaration)
        {
            if (functionDeclaration == null) throw new ArgumentNullException("functionDeclaration");
            if (parameterDeclaration == null) throw new ArgumentNullException("parameterDeclaration");

            return !FunctionContainsAnyNoNullCheckAttribute(functionDeclaration);
        }

        private bool FunctionContainsAnyNoNullCheckAttribute(ICSharpFunctionDeclaration functionDeclaration)
        {
            var extractor = new AttributesExtractor(functionDeclaration);
            var methodAttributes = extractor.GetAttributes();

            _NoNullCheckAttributes = _Settings.GetFunctionAttributesThatIndicatesNoNullCheck();
            return methodAttributes.Any(IsNoNullCheckAttribute);
        }

        private bool IsNoNullCheckAttribute(string attributeName)
        {
            return _NoNullCheckAttributes.Any(a => a.Equals(attributeName,StringComparison.Ordinal));
        }
    }
}
