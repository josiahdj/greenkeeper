﻿using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.ReSharper.Feature.Services.CSharp.Bulbs;
using JetBrains.ReSharper.Psi.CSharp.Tree;
using JetBrains.ReSharper.Psi.Tree;

namespace Greenkeeper.ParameterNullCheck
{
    public static class CSharpFunctionWithParameterFactory 
    {
        public static ICSharpFunctionWithParameterProvider GetFromSelectedElement(ICSharpContextActionDataProvider provider)
        {
            if (provider == null) throw new ArgumentNullException("provider");

            var methodDeclaration = GetFromSelectedElement<IMethodDeclaration>(provider);
            if (methodDeclaration != null)
            {
                return methodDeclaration;
            }

            var ctorDeclaration = GetFromSelectedElement<IConstructorDeclaration>(provider);
            if (ctorDeclaration != null)
            {
                return ctorDeclaration;
            }

            return null;
        }
        
        private static ICSharpFunctionWithParameterProvider GetFromSelectedElement<T>(ICSharpContextActionDataProvider provider)
           where T : class, ICSharpFunctionDeclaration, ICSharpParametersOwnerDeclaration, ITreeNode
        {
            var declaration = provider.GetSelectedElement<T>(false, true);
            if (declaration != null)
            {
                return new CSharpFunctionWithParameterProvider<T>(declaration);
            }
            return null;
        }

        public static ICollection<ICSharpFunctionWithParameterProvider> GetElements(IFile file)
        {
            if (file == null) throw new ArgumentNullException("file");

            var functionWithParameters = new List<ICSharpFunctionWithParameterProvider>();
            functionWithParameters.AddRange(GetElements<IMethodDeclaration>(file));
            functionWithParameters.AddRange(GetElements<IConstructorDeclaration>(file));
            return functionWithParameters;
        }

        private static IEnumerable<ICSharpFunctionWithParameterProvider> GetElements<T>(IFile file)
            where T : class, ICSharpFunctionDeclaration, ICSharpParametersOwnerDeclaration, ITreeNode
        {
            return file.EnumerateSubTree()
                        .OfType<T>()
                        .Select(c => new CSharpFunctionWithParameterProvider<T>(c));
        }
    }
}
