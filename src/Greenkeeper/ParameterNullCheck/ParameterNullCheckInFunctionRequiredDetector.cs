﻿using JetBrains.ReSharper.Psi;
using JetBrains.ReSharper.Psi.CSharp.Tree;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Greenkeeper.ParameterNullCheck
{
    public class ParameterNullCheckInFunctionRequiredDetector
    {
        private readonly ParameterNullCheckSettings _Settings;
        private readonly ICSharpFunctionDeclaration _FunctionDeclaration;
        private readonly ICSharpParametersOwnerDeclaration _ParametersDeclaration;

        public ParameterNullCheckInFunctionRequiredDetector(ICSharpFunctionWithParameterProvider declaration,ParameterNullCheckSettings settings)
        {
            if (declaration == null) throw new ArgumentNullException("declaration");
            if (settings == null) throw new ArgumentNullException("settings");
            _Settings = settings;

            _FunctionDeclaration = declaration.GetFunctionDeclaration();
            _ParametersDeclaration = declaration.GetParametersOwnerDeclaration();

            if (_FunctionDeclaration == null) throw new NullReferenceException("_FunctionDeclaration");
            if (_ParametersDeclaration == null) throw new NullReferenceException("_ParametersDeclaration");
        }

        public bool AnyParameterNeedNullCheck()
        {
            return IsPublic() && AnyParametersWhichHasNotNeededNullCheck();
        }

        private bool IsPublic()
        {
            var accessRights = _FunctionDeclaration.GetAccessRights();
            return accessRights == AccessRights.PUBLIC;
        }

        private bool AnyParametersWhichHasNotNeededNullCheck()
        {
            return YieldParametersToNullCheck().Any(p => p.ParameterHasNotNeededNullCheck);
        }

        public IEnumerable<ParameterToNullCheck> YieldParametersToNullCheck()
        {
            foreach (var parameterDeclaration in _ParametersDeclaration.ParameterDeclarations)
            {
                yield return GetParameterToNullCheck(parameterDeclaration);
            }
        }

        private ParameterToNullCheck GetParameterToNullCheck(ICSharpParameterDeclaration parameterDeclaration)
        {
            var nullCheckDetector = new ParameterNullCheckRequiredDetector(_FunctionDeclaration, parameterDeclaration, _Settings);
            return nullCheckDetector.GetParameterToNullCheck();
        }
    }
}
