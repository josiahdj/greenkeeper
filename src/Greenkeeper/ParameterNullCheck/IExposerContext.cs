﻿namespace Greenkeeper.ParameterNullCheck
{
    public interface IExposerContext
    {
        T GetItem<T>() where T : IExposerContextItem, new();
    }
}