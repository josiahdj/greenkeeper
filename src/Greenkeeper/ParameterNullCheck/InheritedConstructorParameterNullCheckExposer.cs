﻿using JetBrains.Annotations;
using JetBrains.ReSharper.Psi;
using JetBrains.ReSharper.Psi.CSharp.Tree;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using JetBrains.ReSharper.Psi.ExtensionsAPI.Resolve;
using JetBrains.ReSharper.Psi.Impl.Reflection2;

namespace Greenkeeper.ParameterNullCheck
{
    public class InheritedConstructorParameterNullCheckExposer : IStatementNullCheckExposer
    {
        private IConstructorDeclaration _ConstructorDeclaration;
        private InheritedConstructorContextItem _ContextItem;
        private Func<IStatementNullCheckExposer, ICollection<ICSharpStatement>> _Expose;
        private IExposerContext _ExposerContext;
        private ICSharpFunctionDeclaration _FunctionDeclaration;
        private IConstructorDeclaration _InitializersConstructorDeclaration;
        private ICSharpParameterDeclaration _InitializersParameterDeclaration;
        private ICSharpParameterDeclaration _ParameterDeclaration;
        private string _PassedParameterName;

        public ICollection<ICSharpStatement> GetNullCheckForParameterConsiderOnlyCondition(
            [NotNull] ICSharpFunctionDeclaration functionDeclaration,
            [NotNull] ICSharpParameterDeclaration parameterDeclaration,
            [NotNull] IExposerContext exposerContext)
        {
            if (functionDeclaration == null) throw new ArgumentNullException("functionDeclaration");
            if (parameterDeclaration == null) throw new ArgumentNullException("parameterDeclaration");
            if (exposerContext == null) throw new ArgumentNullException("exposerContext");
            _FunctionDeclaration = functionDeclaration;
            _ParameterDeclaration = parameterDeclaration;
            _ExposerContext = exposerContext;
            _Expose = ExposeInheritedNullCheckStatementsConsiderOnlyCondition;
            return GetInheritedNullChecks();
        }

        private ICollection<ICSharpStatement> ExposeInheritedNullCheckStatementsConsiderOnlyCondition(
            IStatementNullCheckExposer nullCheckExposer)
        {
            return nullCheckExposer.GetNullCheckForParameterConsiderOnlyCondition(_InitializersConstructorDeclaration,
                                                                                  _InitializersParameterDeclaration,
                                                                                  _ExposerContext);
        }

        private ICollection<ICSharpStatement> GetInheritedNullChecks()
        {
            if (!CanContainInheritedNullCheck())
            {
                return new Collection<ICSharpStatement>();
            }

            return GetNullChecksFromInitializer();
        }

        private bool CanContainInheritedNullCheck()
        {
            return TryParseConstructorDeclaration() && HasConstructorInitializer() && TryFindPassedParameter();
        }

        private bool TryParseConstructorDeclaration()
        {
            _ConstructorDeclaration = _FunctionDeclaration as IConstructorDeclaration;
            return _ConstructorDeclaration != null;
        }

        private bool HasConstructorInitializer()
        {
            return _ConstructorDeclaration.Initializer != null;
        }

        private bool TryFindPassedParameter()
        {
            foreach (var argument in _ConstructorDeclaration.Initializer.Arguments)
            {
                if (SetPassedParameterNameIfPassedToArgument(argument))
                {
                    return true;
                }
            }
            return false;
        }

        private bool SetPassedParameterNameIfPassedToArgument(ICSharpArgument argument)
        {
            var referenceValue = argument.Value as IReferenceExpression;
            if (referenceValue != null && argument.MatchingParameter != null)
            {
                var name = referenceValue.NameIdentifier.Name;
                if (String.Equals(_ParameterDeclaration.DeclaredName, name))
                {
                    _PassedParameterName = argument.MatchingParameter.Element.ShortName;
                    return true;
                }
            }
            return false;
        }

        private ICollection<ICSharpStatement> GetNullChecksFromInitializer()
        {
            AddConstructorToExposerContext();
            _InitializersConstructorDeclaration = GetInitializersConstructorDeclaration();
            if (_InitializersConstructorDeclaration == null || IsInitializersConstructorAlreadyProcessed())
            {
                return new Collection<ICSharpStatement>();
            }
            _InitializersParameterDeclaration = GetInitializersParameterDeclaration();
            var nullCheckExposer = StatementNullCheckExposerFactory.CreateExposer();
            return _Expose(nullCheckExposer);
        }

        private void AddConstructorToExposerContext()
        {
            SetContextItem();
            _ContextItem.AlreadyProcessedConstructors.Add(_ConstructorDeclaration);
        }
        
        private void SetContextItem()
        {
            _ContextItem = _ExposerContext.GetItem<InheritedConstructorContextItem>();
        }

        private IConstructorDeclaration GetInitializersConstructorDeclaration()
        {
            var initializer = _ConstructorDeclaration.Initializer;
            if (initializer == null) throw new NullReferenceException("initializer");
            if (initializer.Reference == null) throw new NullReferenceException("initializer.Reference");

            var resolvedReference = initializer.Reference.Resolve();
            if (resolvedReference.DeclaredElement == null)
                throw new NotSupportedException("resolved initializer has no declared element");


            if (IsFromExternalAssembly(resolvedReference))
            {
                return null;
            }
            
            return resolvedReference.DeclaredElement
                                    .GetDeclarations()
                                    .OfType<IConstructorDeclaration>()
                                    .Single();
        }

        private static bool IsFromExternalAssembly(ResolveResultWithInfo resolvedReference)
        {
            return resolvedReference.DeclaredElement is ICompiledElement;
        }

        private bool IsInitializersConstructorAlreadyProcessed()
        {
            return _ContextItem.AlreadyProcessedConstructors.Any(c => ReferenceEquals(c, _InitializersConstructorDeclaration));
        }

        private ICSharpParameterDeclaration GetInitializersParameterDeclaration()
        {
            foreach (var param in _InitializersConstructorDeclaration.ParameterDeclarations)
            {
                if (param.DeclaredName.Equals(_PassedParameterName))
                {
                    return param;
                }
            }
            throw new NotSupportedException("passed parametername '" + _PassedParameterName +
                                            "' not found in parameterlist");
        }

        public ICollection<ICSharpStatement> GetNullChecksForParameter(
            [NotNull] ICSharpFunctionDeclaration functionDeclaration,
            [NotNull] ICSharpParameterDeclaration parameterDeclaration,
            [NotNull] IExposerContext exposerContext)
        {
            if (functionDeclaration == null) throw new ArgumentNullException("functionDeclaration");
            if (parameterDeclaration == null) throw new ArgumentNullException("parameterDeclaration");
            if (exposerContext == null) throw new ArgumentNullException("exposerContext");
            _FunctionDeclaration = functionDeclaration;
            _ParameterDeclaration = parameterDeclaration;
            _ExposerContext = exposerContext;
            _Expose = ExposeInheritedNullCheckStatements;
            return GetInheritedNullChecks();
        }

        private ICollection<ICSharpStatement> ExposeInheritedNullCheckStatements(IStatementNullCheckExposer nullCheckExposer)
        {
            return nullCheckExposer.GetNullChecksForParameter(_InitializersConstructorDeclaration,
                                                              _InitializersParameterDeclaration,
                                                              _ExposerContext);
        }
    }
}
