﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using JetBrains.Annotations;
using JetBrains.Application.Settings;
using JetBrains.ReSharper.Feature.Services.Bulbs;
using JetBrains.ReSharper.Feature.Services.CSharp.Bulbs;
using JetBrains.ReSharper.Intentions.Extensibility;
using JetBrains.ReSharper.Intentions.Extensibility.Menu;
using JetBrains.ReSharper.Psi.CSharp.Tree;
using JetBrains.ReSharper.Psi.Tree;
using JetBrains.UI.Avalon.TreeListView;
using JetBrains.Util;
using NLog;
using System;
using Logger = NLog.Logger;

namespace Greenkeeper.ParameterNullCheck
{
    [ContextAction(Group = "C#", Name = ParameterNullCheckBulbAction.CheckAllParamsForNullName, Description = ParameterNullCheckBulbAction.CheckAllParamsForNullDescription)]
    public class ParameterNullCheckContextAction : IContextAction
    {
        private readonly ICSharpContextActionDataProvider _Provider;
        private readonly Logger _Logger;
        private readonly IContextBoundSettingsStore _SettingsStore;
        private ParameterNullCheckSettings _Settings;

        public ParameterNullCheckContextAction(ICSharpContextActionDataProvider provider)
        {
            if (provider == null) throw new ArgumentNullException("provider");

            _Provider = provider;
            _SettingsStore = _Provider.PsiServices.SettingsStore.BindToContextTransient(ContextRange.ApplicationWide);
            _Logger = LogManager.GetCurrentClassLogger();
        }

        public static ParameterNullCheckContextAction Create([NotNull] ICSharpContextActionDataProvider provider, [NotNull] ParameterNullCheckSettings settings)
        {
            if (provider == null) throw new ArgumentNullException("provider");
            if (settings == null) throw new ArgumentNullException("settings");
            var nullCheckContextAction = new ParameterNullCheckContextAction(provider);
            nullCheckContextAction._Settings = settings;
            return nullCheckContextAction;
        }

        private void LoadParameterNullCheckSettings()
        {
            var loader = new ParameterNullCheckSettingsLoader(_SettingsStore);
            _Settings = loader.Load();
        }

        private IBulbAction GetParameterNullCheckBulbItems()
        {
            var declaration = GetSelectedFunctionWithParameter();
            LoadSettingsIfNotLoaded();
            return new ParameterNullCheckBulbAction(declaration, _Settings);
        }

        private void LoadSettingsIfNotLoaded()
        {
            if (_Settings == null)
            {
                LoadParameterNullCheckSettings();
            }
        }

        private string GetContextActionLogDescription()
        {
            var declaration = GetSelectedFunctionWithParameter();
            var functionPath = string.Empty;
            if (declaration != null)
            {
                var containingFileName = GetContainingFileName(declaration.GetFunctionDeclaration());
                functionPath = containingFileName + "." + declaration.GetFunctionDeclaration().DeclaredName;
            }

            return "NullCheckContextActionIsAvailable on " + functionPath;
        }

        private static string GetContainingFileName(ITreeNode treeNode)
        {
            var containingFileName = string.Empty;
            var containingFile = treeNode.GetContainingFile();
            if (containingFile != null)
            {
                var sourceFile = containingFile.GetSourceFile();
                if (sourceFile != null)
                {
                    containingFileName = sourceFile.Name;
                }
            }
            return containingFileName;
        }

        public IEnumerable<IntentionAction> CreateBulbItems()
        {
            return GetParameterNullCheckBulbItems().ToContextAction();
        }

        public bool IsAvailable(IUserDataHolder cache)
        {
            return ExceptionLogger.Execute(_Logger, GetContextActionLogDescription, () => IsNullCheckContextActionAvailableAndLogTrackedTime());
        }

        private bool IsNullCheckContextActionAvailableAndLogTrackedTime()
        {
            return TimeTrackerLogger.Execute(_Logger, LogLevel.Info, GetContextActionLogDescription, () => IsNullCheckContextActionAvailable());
        }

        private bool IsNullCheckContextActionAvailable()
        {
            return IsCaretInParameterList() && NeedNullCheck();
        }

        private bool NeedNullCheck()
        {
            var declaration = GetSelectedFunctionWithParameter();
            if (declaration != null)
            {
                return AnyNeedNullCheckForDeclaration(declaration);
            }
            return false;
        }

        private bool AnyNeedNullCheckForDeclaration(ICSharpFunctionWithParameterProvider declaration)
        {
            LoadSettingsIfNotLoaded();
            var detector = new ParameterNullCheckInFunctionRequiredDetector(declaration, _Settings);
            return detector.AnyParameterNeedNullCheck();
        }

        private ICSharpFunctionWithParameterProvider GetSelectedFunctionWithParameter()
        {
            return CSharpFunctionWithParameterFactory.GetFromSelectedElement(_Provider);
        }

        private bool IsCaretInParameterList()
        {
            var node = _Provider.GetSelectedElement<IFormalParameterList>(false, true);
            return node != null;
        }
    }
}
