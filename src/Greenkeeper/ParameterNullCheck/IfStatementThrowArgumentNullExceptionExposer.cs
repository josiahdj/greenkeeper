﻿using System;
using System.Linq;
using JetBrains.ReSharper.Psi.CSharp.Parsing;
using JetBrains.ReSharper.Psi.CSharp.Tree;

namespace Greenkeeper.ParameterNullCheck
{
    public class IfStatementThrowArgumentNullExceptionExposer
    {
        private const string _ArgumentNullExceptionName = "ArgumentNullException";
        private const string _DoubleQuotes = "\"";

        private ICSharpStatement _CSharpStatement;
        private ICSharpExpression _Condition;
        private IIfStatement _IfStatement;
        private IThrowStatement _ThrowStatement;
        private ICSharpLiteralExpression _ThrowedParameter;

        public IfStatementThrowArgumentNullExceptionData Expose(ICSharpStatement cSharpStatement)
        {
            if (cSharpStatement == null) throw new ArgumentNullException("cSharpStatement");
            SetStatementParts(cSharpStatement);
            var paramName = GetThrowedParameterName();
            return new IfStatementThrowArgumentNullExceptionData(IsValidThrowStatement(), _Condition, paramName);
        }

        private void SetStatementParts(ICSharpStatement cSharpStatement)
        {
            _CSharpStatement = cSharpStatement;
            _IfStatement = _CSharpStatement as IIfStatement;

            if (_IfStatement != null)
            {
                _Condition = _IfStatement.Condition;
                SetThrowStatement();
                SetThrowedParameter();
            }
        }

        private void SetThrowStatement()
        {
            _ThrowStatement = _IfStatement.Then as IThrowStatement;
            if (_ThrowStatement != null)
            {
                return;
            }

            SetThrowStatementFromBlock();
        }

        private void SetThrowStatementFromBlock()
        {
            var block = _IfStatement.Then as IBlock;
            if (block == null)
            {
                return;
            }

            var firstStatement = block.Statements.FirstOrDefault();
            if (firstStatement == null)
            {
                return;
            }

            _ThrowStatement = firstStatement as IThrowStatement;
        }

        private void SetThrowedParameter()
        {
            if (_ThrowStatement != null)
            {
                var objCreationExpression = _ThrowStatement.Exception as IObjectCreationExpression;
                if (objCreationExpression != null && IsThrowingArgumentNullException(objCreationExpression))
                {
                    TrySetParameterFromExceptionArgument(objCreationExpression);
                }
            }
        }

        private static bool IsThrowingArgumentNullException(IObjectCreationExpression objCreationExpression)
        {
            return objCreationExpression.TypeName != null &&
                   String.Equals(objCreationExpression.TypeName.ShortName, _ArgumentNullExceptionName);
        }

        private void TrySetParameterFromExceptionArgument(IObjectCreationExpression objCreationExpression)
        {
            if (objCreationExpression.Arguments.Count > 0)
            {
                var firstArg = objCreationExpression.Arguments.First();
                _ThrowedParameter = GetExceptionArgumentParameter(firstArg);
            }
        }

        private ICSharpLiteralExpression GetExceptionArgumentParameter(ICSharpArgument argument)
        {
            var argLiteral = argument.Value as ICSharpLiteralExpression;
            if (argLiteral != null && IsStringLiteral(argLiteral))
            {
                return argLiteral;
            }
            return null;
        }

        private bool IsStringLiteral(ICSharpLiteralExpression argLiteral)
        {
            return argLiteral.Literal != null && argLiteral.Literal.GetTokenType() == CSharpTokenType.STRING_LITERAL;
        }

        private string GetThrowedParameterName()
        {
            if (_ThrowedParameter == null)
            {
                return null;
            }

            var parameterText = _ThrowedParameter.GetText();
            if (parameterText == null)
            {
                return null;
            }
            return parameterText.Replace(_DoubleQuotes, string.Empty);;
        }

        private bool IsValidThrowStatement()
        {
            return _ThrowStatement != null && _ThrowedParameter != null;
        }
    }
}

