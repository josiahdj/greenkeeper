﻿using NLog;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;

namespace Greenkeeper
{
    public class TimeTrackerLogger
    {
        private const string _MilliSecondsShort = "ms";
        private readonly Logger _Logger;
        private readonly LogLevel _LogLevel;
        private readonly Func<string> _GetMessage;
        private TimeSpan _TrackedTime;

        public TimeTrackerLogger(Logger logger, LogLevel logLevel, Func<string> getMessage)
        {
            _Logger = logger;
            _LogLevel = logLevel;
            _GetMessage = getMessage;
        }

        public static void Execute(Logger logger, LogLevel logLevel, Func<string> getMessage, Action toExecute)
        {
            Execute(logger, logLevel, getMessage, () => ExecuteWithFakeResult(toExecute));
        }

        private static bool ExecuteWithFakeResult(Action toExecute)
        {
            toExecute();
            return true;
        }

        public static T Execute<T>(Logger logger, LogLevel logLevel, Func<string> getMessage, Func<T> toExecute)
        {
            var trackerLogger = new TimeTrackerLogger(logger, logLevel, getMessage);
            return trackerLogger.Execute(toExecute);
        }

        public T Execute<T>(Func<T> toExecute)
        {
            var result = ExecuteAndTrackTime(toExecute);
            LogTrackedTime();
            return result;
        }

        private T ExecuteAndTrackTime<T>(Func<T> toExecute)
        {
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            var result = toExecute();
            stopWatch.Stop();
            _TrackedTime = stopWatch.Elapsed;
            return result;
        }

        private void LogTrackedTime()
        {
            _Logger.Log(_LogLevel, GenerateMessage(_TrackedTime));
        }

        private string GenerateMessage(TimeSpan timeSpan)
        {
            return _GetMessage() + " (" + GetReadableTimeSpan(timeSpan)+")";
        }

        private static string GetReadableTimeSpan(TimeSpan span)
        {
            var notEmptyParts = GetNotEmptyTimeSpanParts(span);
            if (notEmptyParts.Count == 0)
            {
                notEmptyParts.Add(span.TotalMilliseconds+_MilliSecondsShort);
            }
            return String.Join(" ", notEmptyParts);
        }

        private static ICollection<string> GetNotEmptyTimeSpanParts(TimeSpan span)
        {
            return GetTimeSpanParts(span).Where(p => !p.StartsWith("0")).ToList();
        }

        private static ICollection<string> GetTimeSpanParts(TimeSpan span)
        {
            return new Collection<string>
                {
                    span.Days + "d",
                    span.Hours + "h",
                    span.Minutes + "m",
                    span.Seconds + "s",
                    span.Milliseconds + _MilliSecondsShort
                };

            
        }
    }
}