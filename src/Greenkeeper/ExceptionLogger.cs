﻿using NLog;
using System;

namespace Greenkeeper
{
    public class ExceptionLogger
    {
        private readonly Logger _Logger;
        private readonly Func<string> _GetMessage;

        public static void Execute(Logger logger, Func<string> getMessage, Action toExecute)
        {
            Execute(logger, getMessage, () => ExecuteWithFakeResult(toExecute));
        }

        private static bool ExecuteWithFakeResult(Action toExecute)
        {
            toExecute();
            return true;
        }

        public static T Execute<T>(Logger logger, Func<string> getMessage, Func<T> toExecute)
        {
            var exceptionLogger = new ExceptionLogger(logger, getMessage);
            return exceptionLogger.Execute(toExecute);
        }

        public ExceptionLogger(Logger logger, Func<string> getMessage)
        {
            _GetMessage = getMessage;
            _Logger = logger;
        }

        public T Execute<T>(Func<T> toExecute)
        {
            try
            {
                return toExecute();
            }
            catch (Exception ex)
            {
                _Logger.ErrorException(GetMessage(), ex);
                throw;
            }
        }

        private string GetMessage()
        {
            try
            {
                return _GetMessage();
            }
            catch (Exception ex)
            {
                const string errorGettingMessage = "Error getting Message";
                _Logger.ErrorException(errorGettingMessage, ex);
                return "<" + errorGettingMessage + ">";
            }
        }
    }
}
