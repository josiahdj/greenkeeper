﻿using System;
using JetBrains.Annotations;
using JetBrains.ReSharper.Daemon;
using JetBrains.ReSharper.Psi;
using JetBrains.ReSharper.Psi.CSharp;
using JetBrains.ReSharper.Psi.Files;
using JetBrains.ReSharper.Psi.Tree;

namespace Greenkeeper
{
    public static class PsiFileFinder
    {
        public static IFile FindCSharpPsiFile([NotNull] IDaemonProcess process)
        {
            if (process == null) throw new ArgumentNullException("process");
            var sourceFile = process.SourceFile;
            return FindCSharpPsiFile(sourceFile);
        }

        public static IFile FindCSharpPsiFile([NotNull] IPsiSourceFile sourceFile)
        {
            if (sourceFile == null) throw new ArgumentNullException("sourceFile");
            var psiServices = sourceFile.GetPsiServices();
            return psiServices.Files.GetDominantPsiFile<CSharpLanguage>(sourceFile);
        }
    }
}
