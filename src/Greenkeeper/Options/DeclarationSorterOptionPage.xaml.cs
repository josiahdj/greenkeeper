﻿using Greenkeeper.DeclarationSorting;
using Greenkeeper.DeclarationSorting.Serialization;
using JetBrains.Annotations;
using JetBrains.Application.Settings;
using JetBrains.DataFlow;
using JetBrains.UI.CrossFramework;
using JetBrains.UI.Options;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Navigation;

namespace Greenkeeper.Options
{

  [OptionsPage(_PageId, "Sorting", typeof(UnnamedThemedIcons.GreenkeeperSort), ParentId = RootOptionPage.PageId)]
  public partial class DeclarationSorterOptionPage : IOptionsPage
  {
        private const string _PageId = "GreenkeeperDeclarationSorterOptions";
        private OptionsSettingsSmartContext _SettingsStore;
        private DeclarationSorterSettings _Settings;

      public DeclarationSorterOptionPage([NotNull] Lifetime lifetime, [NotNull] OptionsSettingsSmartContext settings)
        {
          if (lifetime == null) throw new ArgumentNullException("lifetime");
          if (settings == null) throw new ArgumentNullException("settings");
          InitializeComponent();

          _SettingsStore = settings;
            Loaded += OnDeclarationSorterOptionPageLoaded;
        }

        private void OnDeclarationSorterOptionPageLoaded(object sender, RoutedEventArgs e)
        {
            LoadSettingsFromStore();
            SetSettingsToControls();
        }

        private void LoadSettingsFromStore()
        {
            var loader = new DeclarationSorterSettingsLoader(_SettingsStore);
            _Settings = loader.Load();
        }
      
        private void SetSettingsToControls()
        {
            SetXmlSorterDefinition(_Settings.SerializedDeclarationSorterDefinition);
            cbUseXmlDefinition.IsChecked = _Settings.UseDeclarationSorterDefinition;
        }

        public EitherControl Control
        {
          get { return this; }
        }

        public string Id
        {
            get { return _PageId; }
        }

        public bool OnOk()
        {
            if (IsXmlDefinitionValidIfNotShowMessageBox())
            {
                SaveXmlDefinitionToStore();
                return true;
            }
            return false;
        }
      
        private bool IsXmlDefinitionValidIfNotShowMessageBox()
        {
            var settings = GetDeclarationSorterSettings();
            var checker = new DeclarationSorterSettingsChecker();
            bool valid = checker.IsSerializedDefinitionValid(settings);
            if (!valid)
            {
                ShowInvalidXmlMessageBox(checker.GetErrorMessage());
            }
            return valid;
        }

        private void ShowInvalidXmlMessageBox(string error)
        {
            MessageBox.Show("Xml not valid." + Environment.NewLine + error, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
        }
      
        private DeclarationSorterSettings GetDeclarationSorterSettings()
        {
            return new DeclarationSorterSettings
            {
                SerializedDeclarationSorterDefinition = txtXmlDefinition.Text,
                UseDeclarationSorterDefinition = UseXmlDefinitionChecked()
            };
        }

      private bool UseXmlDefinitionChecked()
      {
          return cbUseXmlDefinition.IsChecked.HasValue && cbUseXmlDefinition.IsChecked.Value;
      }

      private void SaveXmlDefinitionToStore()
        {
            var settings = GetDeclarationSorterSettings();
            _SettingsStore.SetKey(settings, SettingsOptimization.OptimizeDefault);
        }

        public bool ValidatePage()
        {
          return true;
        }

      private void OnHyperlinkRequestNavigate(object sender, RequestNavigateEventArgs e)
      {
          Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));
          e.Handled = true;
      }

      private void OnClickSetDefault(object sender, RoutedEventArgs e)
      {
          SetXmlSorterDefinition(DefaultDeclarationSortingDefinitionResource.DefaultDeclarationSortingDefinition);
      }

      private void SetXmlSorterDefinition(string xml)
      { 
          txtXmlDefinition.Text = xml;
      }

      private void OnClickSetStyleCop(object sender, RoutedEventArgs e)
      {
          SetXmlSorterDefinition(DefaultDeclarationSortingDefinitionResource.StyleCopDeclarationSortingDefinition);

      }
  }
}
