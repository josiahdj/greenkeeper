﻿using JetBrains.ReSharper.Features.Common.Options;
using JetBrains.UI.CrossFramework;
using JetBrains.UI.Options;

namespace Greenkeeper.Options
{
    [OptionsPage(PageId, "Greenkeeper", typeof(UnnamedThemedIcons.Greenkeeper), ParentId = ToolsPage.PID)]
    public class RootOptionPage : IOptionsPage
    {
        public const string PageId = "GreenkeeperRootOptions";
        
        public EitherControl Control
        {
            get { return null; }
        }
        
        public string Id
        {
            get { return "Tools"; }
        }

        public bool OnOk()
        {
            return true;
        }
       
        public bool ValidatePage()
        {
            return true;
        }
    }
}
