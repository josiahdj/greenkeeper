﻿using System;
using JetBrains.ProjectModel;
using JetBrains.ReSharper.Psi.CSharp;
using JetBrains.ReSharper.Psi.Tree;

namespace Greenkeeper
{
    public abstract class ElementWriter : TextWriter
    {
        public readonly CSharpElementFactory ElementFactory;
        protected readonly ITreeNode Element;

        protected ElementWriter(ITreeNode element, ISolution solution)
            : base(solution)
        {
            if (element == null) throw new ArgumentNullException("element");
            Element = element;
            ElementFactory = CSharpElementFactory.GetInstance(Element.GetPsiModule());
            if (ElementFactory == null) throw new ArgumentException("CSharpElementFactory.GetInstance() returns null");
        }

        protected void InsertAtIndexTemplateMethod(object toInsert, int index)
        {
            if (IsEmpty())
            {
                InsertWithLock(toInsert);
                return;
            }

            if (IsEndOfElement(index))
            {
                InsertAfterLastWithLock(toInsert);
                return;
            }

            InsertBeforeWithLock(toInsert, index);
        }

        private void InsertWithLock(object toInsert)
        {
            EnsureWriteWithLock(() => Insert(toInsert));
        }

        private void InsertAfterLastWithLock(object toInsert)
        {
            EnsureWriteWithLock(() => InsertAfterLast(toInsert));
        }

        private void InsertBeforeWithLock(object toInsert, int index)
        {
            EnsureWriteWithLock(() => InsertBefore(toInsert, index));
        }

        protected abstract bool IsEmpty();
        protected abstract bool IsEndOfElement(int index);
        protected abstract void Insert(object toInsert);
        protected abstract void InsertAfterLast(object toInsert);
        protected abstract void InsertBefore(object toInsert, int index);
    }
}
